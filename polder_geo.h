/*---
 *
 * polder_geo
 *
 * Copyright (c) 2007 by
 *
 * Francois Thieuleux (thieul@loa.univ-lille1.fr)
 * Fabrice Ducos (ducos@loa.univ-lille1.fr)
 *
 * Laboratoire d'Optique Atmospherique
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * ---*/

/* $Id$ */

#ifndef POLDER_GEO_H
#define POLDER_GEO_H

/* */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h> /* memset */
#include <unistd.h> /* getopt */
#include <assert.h>
#include <libgen.h>

#define N_LINES_FULL_RESOLUTION 3240
#define N_LINES_MEDIUM_RESOLUTION 1080
#define N_LINES_LOW_RESOLUTION 360

#define POLDER_FULL_RESOLUTION 1
#define POLDER_MEDIUM_RESOLUTION 2
#define POLDER_LOW_RESOLUTION 3

/* returns 0 on success
 *
 * valid values for the 'res' argument are:
 * POLDER_FULL_RESOLUTION   (1)
 * POLDER_MEDIUM_RESOLUTION (2)
 * POLDER_LOW_RESOLUTION    (3)
 */
extern int latlon2ligcol_polder(double lat, double lon, int res, double *lig, double *col);
extern int ligcol2latlon_polder(double lig, double col, int res, double *lat, double *lon);

#endif /* POLDER_GEO_H */
