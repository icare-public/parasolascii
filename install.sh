#!/bin/sh

if [ -z "$HOME" ] ; then
    echo "you seem not to be on a Unix-like system. If you're on Windows, run install-win.sh instead" 1>&2
    exit 1
fi

choice_1="/usr/local"
choice_2="$HOME"

cat << END_OF_CAT
Where do you wish parasolascii to be installed ?
  0) abort installation
  1) $choice_1
  2) $choice_2
  3) a directory of your choice
END_OF_CAT

echo -n "your choice: "
read choice

case $choice in
    0) exit 0 ;;
    1) INSTALL_DIR=$choice_1/bin
       PARAM_DIR="/etc/parasolascii" ;;
    2) INSTALL_DIR=$choice_2/bin
       PARAM_DIR="$HOME/.parasolascii" ;;
    3) echo -n "which directory: "
       read INSTALL_DIR
       PARAM_DIR="$INSTALL_DIR/.parasolascii" ;;
    *) echo "bad choice (0, 1, 2 or 3 only)" 1>&2
       exit 1 ;;
esac


 # cleans up eventual old installations
[ -d "$INSTALL_DIR/.parasolascii" ] && { rm -rf "$INSTALL_DIR/.parasolascii" ; } # for versions below 1.9.20
[ -d "$PARAM_DIR" ] && { rm -rf "$PARAM_DIR" ; }

[ -d "$INSTALL_DIR" ] || { mkdir -p "$INSTALL_DIR" || exit 1 ; }
[ -d "$PARAM_DIR" ] || { mkdir -p "$PARAM_DIR" || exit 1 ; }

export INSTALL_DIR
export PARAM_DIR
make clean
make

cp PARAM_*.txt "$PARAM_DIR"
chmod 555 "$PARAM_DIR"
chmod 444 "$PARAM_DIR"/PARAM_*.txt
chmod -R u+w "$PARAM_DIR"
cp polderascii parasolascii ascii2map_full ascii2map_medium parasolview_full parasolview_medium "$INSTALL_DIR"
make clean

echo
echo
echo "=============================================================================================="
echo
echo "The following executables have been installed into $INSTALL_DIR :"
echo "polderascii parasolascii ascii2map_full ascii2map_medium parasolview_full parasolview_medium"
echo "(polderascii is just an alias for parasolascii, kept for backward"
echo "compatibility reasons, you can use one or the other interchangeably but parasolascii is"
echo "recommended now)."
echo
echo "check your PATH environment variable (with \"echo \$PATH\" for instance)"
echo "and add $INSTALL_DIR to it if it's not already done,"
echo "so you can use the command from anywhere"
echo
echo "One method to do this : add the following commands to the files below, then log again"
echo "export PATH=\$PATH:$INSTALL_DIR (in .profile or .bash_profile, if you use sh/ksh/bash/zsh)"
echo "setenv PATH \$PATH:$INSTALL_DIR (in .login, if you use csh/tcsh)"
echo
echo "=============================================================================================="
echo
echo "You can easily uninstall the software by hand by removing the above executables"
echo "and the directory $PARAM_DIR"
echo
echo "DO NOT move or remove $PARAM_DIR"
echo "as long as you use the software,you would prevent it from working."
echo
echo "=============================================================================================="
exit 0

