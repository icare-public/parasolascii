/*---
 *
 * polder_geo
 *
 * Copyright (c) 2007 by
 *
 * Francois Thieuleux (thieul@loa.univ-lille1.fr)
 * Fabrice Ducos (ducos@loa.univ-lille1.fr)
 *
 * Laboratoire d'Optique Atmospherique
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * ---*/

/* $Id$ */

#include "polder_geo.h"

/* Veersion number */
#define VERSION "0.1.1"

/********************************************************************************/
/*                                                                              */
/* This function returns the nearest integer value from a decimal value,        */
/* as does the homonym intrinsic Fortran function.                              */
/* Usefull for latlon2ligcol_polder() and ligcol2latlon_polder() functions.     */
/*                                                                              */
/*   double x    [IN]  : Decimal value                                          */
/*                                                                              */
/********************************************************************************/
static int NINT(double x) { return x < 0.0 ? (int)(x-0.5):(int)(x+0.5); }


/********************************************************************************/
/*                                                                              */
/*  This function computes line and column coordinates in the POLDER sinusoidal */
/*  reference grid, from geographical coordinates.                              */
/*  The function returns 0 when success, 1 when resolution is not supported,    */
/*  2 when geographical coordinates are inconsistent.                           */
/*                                                                              */
/*   The reference POLDER grid origin pixel's coordinate is (1,1)               */
/*                                                                              */
/*   double lat   [IN]  : Geographical latitude (degrees), postive northward    */
/*   double lon   [IN]  : Geographical longitude (positive eastward)            */
/*   int res     [IN]  : Resolution of the reference grid. Value can be:        */
/*              - POLDER_FULL_RESOLUTION (1)   : full resolution grid (1x1)     */
/*              - POLDER_MEDIUM_RESOLUTION (2) : medium resolution grid (3x3)   */
/*              - POLDER_LOW_RESOLUTION (3)    : low resolution grid (9x9)      */
/*                                                                              */
/*   double *lig  [OUT] : Address of the value of the line in the POLDER        */
/*                       reference grid at the corresponding resolution         */
/*                       (see res argument).                                    */
/*   double *col  [OUT] : Address of the value of the column in the POLDER      */
/*                       reference grid at the corresponding resolution         */
/*                       (see res argument).                                    */
/*                                                                              */
/*                                                                              */
/********************************************************************************/
int latlon2ligcol_polder(double lat, double lon, int res, double *lig, double *col) {
  int nlines;      /* Absolute lines numbers int the POLDER grid for the requested reolsution res */
  double Ni;       /* Intermediate variables */

  switch (res) {
    case POLDER_FULL_RESOLUTION   : nlines=N_LINES_FULL_RESOLUTION; break; /* Full resolution (1x1) */
    case POLDER_MEDIUM_RESOLUTION : nlines=N_LINES_MEDIUM_RESOLUTION; break; /* Medium resolution (3x3) */
    case POLDER_LOW_RESOLUTION    : nlines=N_LINES_LOW_RESOLUTION; break; /* Low resolution (9x9) */
    default                : fprintf(stderr,"Output POLDER grid resolution not supported\n"); return(1); /* Error case */
  } /* switch */
  
  if ( (lat > +90.) || (lat < -90.) || (lon > +180.) || (lon < -180.) ) {
    fprintf(stderr,"Invalid physical coordinates for the specified (%f,%f) input geographical coordinate couple\n",lat,lon);
    *lig=-99.;
    *col=-99.;
    return(2);
  } /* Test on input validity space domain */

  *lig = (nlines/180.)*(90.-lat)+0.5;
  Ni   = NINT(nlines*sin(M_PI*(*lig-0.5)/nlines));
  *col = nlines+0.5+Ni*lon/180.;

  return 0;	/* Nominal output */
} /* latlon2ligcol_polder */

/********************************************************************************/
/*                                                                              */
/*  This function computes latitude and longitude geographical coordinates      */
/*  from line and column in the POLDER sinusoidal reference grid.               */
/*  The function returns 0 when success, 1 when resolution is not supported,    */
/*  2 when geographical coordinates are inconsistent.                           */
/*                                                                              */
/*   The reference POLDER grid origin pixel's coordinate is (1,1)               */
/*                                                                              */
/*   double lig  [IN]   : Value of the line in the POLDER                       */
/*                       reference grid at the corresponding resolution         */
/*                       (see res argument).                                    */
/*   double col  [IN]   : Value of the column in the POLDER                     */
/*                       reference grid at the corresponding resolution         */
/*                       (see res argument).                                    */
/*   int res     [IN]  : Resolution of the reference grid. Value can be:        */
/*              - POLDER_FULL_RESOLUTION (1)   : full resolution grid (1x1)     */
/*              - POLDER_MEDIUM_RESOLUTION (2) : medium resolution grid (3x3)   */
/*              - POLDER_LOW_RESOLUTION (3)    : low resolution grid (9x9)      */
/*   double *lat  [OUT] : Address of the geographical latitude (degrees),       */
/*                       postive northward                                      */
/*   double *lon  [OUT] : Address of the geographical longitude ((degrees),     */
/*                       positive eastward                                      */
/*                                                                              */
/*                                                                              */
/*                                                                              */
/*                                                                              */
/********************************************************************************/
int ligcol2latlon_polder(double lig, double col, int res, double *lat, double *lon) {
  int nlines;      /* Absolute lines numbers int the POLDER grid for the requested resolution res */
  double Ni;       /* Intermediate variables */

  switch (res) {
    case POLDER_FULL_RESOLUTION   : nlines=N_LINES_FULL_RESOLUTION; break; /* Full resolution (1x1) */
    case POLDER_MEDIUM_RESOLUTION : nlines=N_LINES_MEDIUM_RESOLUTION; break; /* Medium resolution (3x3) */
    case POLDER_LOW_RESOLUTION    : nlines=N_LINES_LOW_RESOLUTION; break; /* Low resolution (9x9) */
    default                : fprintf(stderr,"Output POLDER grid resolution not supported\n"); return(1); /* Error case */
  } /* switch */

  if ((lig < 0) || (lig > nlines) || (abs(col-0.5-nlines)>NINT(nlines*(sin(M_PI*(lig-0.5)/nlines))))) {
    fprintf(stderr,"Invalid physical coordinates for the specified (%f,%f) input grid coordinate couple\n",lig,col);
    *lat=777.777;
    *lon=777.777;
    return(2);
  } /* Test on input validity space domain */

  *lat = 90.-180./nlines*(lig-0.5);
  Ni   = NINT(nlines*cos(M_PI*(*lat)/180.));
  *lon = 180.*(col-nlines-0.5)/Ni;

  return 0;      /* Nominal output */
} /* ligcol2latlon_polder */


/*
int main() {

int err;

double lat;
double lon;

int res;

double lig;
double col;

lat=87.25;
lon=170.0;

res=POLDER_FULL_RESOLUTION;

printf("INPUT  lat:[%f] lon:[%f]\n",lat,lon);
err=latlon2ligcol_polder(lat,lon,res,&lig,&col);
printf("OUTPUT lig:[%f] col:[%f]\n",lig,col);
printf("err:[%d]\n",err);

printf("INPUT  lig:[%f] col:[%f]\n",lig,col);
err=ligcol2latlon_polder(lig,col,res,&lat,&lon);
printf("OUTPUT lat:[%f] lon:[%f]\n",lat,lon);
printf("err:[%d]\n",err);

lig=1620;
col=3240;

printf("INPUT  lig:[%f] col:[%f]\n",lig,col);
err=ligcol2latlon_polder(lig,col,res,&lat,&lon);
printf("OUTPUT lat:[%f] lon:[%f]\n",lat,lon);
printf("err:[%d]\n",err);

printf("INPUT  lat:[%f] lon:[%f]\n",lat,lon);
err=latlon2ligcol_polder(lat,lon,res,&lig,&col);
printf("OUTPUT lig:[%f] col:[%f]\n",lig,col);
printf("err:[%d]\n",err);


}

*/
