/* 
Copyright or Copr. Fabrice Ducos, CGTD Icare, Laboratoire
d'Optique Atmospherique, 4th May 2005, 23 nov 2007

fabrice.ducos@icare.univ-lille1.fr
ducos@loa.univ-lille1.fr

This software is a computer program whose purpose is to read
PARASOL data format.

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h> /* memset */
#include <unistd.h> /* getopt */
#include <limits.h>
#include <math.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
#include <libgen.h> /* basename */
#include "parasolascii.h"
#include "polder_geo.h"

#define FALSE 0
#define TRUE 1

#define min(a, b) ( (a) < (b) ? (a) : (b) )
#define max(a, b) ( (a) < (b) ? (b) : (a) )
#define MAX_STRING_LENGTH 1023
#define MAX_OUTPUT_LENGTH 65535
#define LEVEL1_SCALING_FACTORS_OFFSET 169380
#define LEVEL2_SCALING_FACTORS_OFFSET 3060
#define LEVEL3_SCALING_FACTORS_OFFSET 1260
#define DATA_RECORD_OFFSET 180
#define LEVEL1_LEADER_ANNOTATION_OFFSET 182520
#define LEVEL2_LEADER_ANNOTATION_OFFSET 16200
#define LEVEL3_LEADER_ANNOTATION_OFFSET 14400

#define NMAX_PARAMS 1024

#define N_ROWS_MEDIUMRES 1080
#define N_COLUMNS_MEDIUMRES 2160
#define N_ROWS_FULLRES 3240
#define N_COLUMNS_FULLRES 6480

#define RAW_MODE 2
#define DEBUG_MODE 4
#define COORD_PARASOL 8
#define COORD_LATLON 16
#define COORD_REVERSED 32
#define COORD_TIME 64
#define COORD_NODE_LON 128
#define OUTPUT_MISSING 256
#define OUTPUT_OUT_OF_RANGE 512
#define DQX_DECIMAL 1024

#define version "1.9.18"

#define MEDIUM_RESOLUTION 1
#define FULL_RESOLUTION 2

#define PARASOL_LEVEL1 1
#define PARASOL_LEVEL2 2
#define PARASOL_LEVEL3 3

#define PARASOL_LEVEL1_UI1_MISSING 0
#define PARASOL_LEVEL1_SI1_MISSING -127
#define PARASOL_LEVEL1_UI2_MISSING 0
#define PARASOL_LEVEL1_SI2_MISSING -32767
#define PARASOL_LEVEL1_SI2_OUT_OF_RANGE 32767
#define PARASOL_LEVEL2_UI1_MISSING 255
#define PARASOL_LEVEL2_UI2_MISSING 65535
#define PARASOL_LEVEL2_UI1_OUT_OF_RANGE 254
#define PARASOL_LEVEL2_UI2_OUT_OF_RANGE 65534
#define PARASOL_LEVEL3_UI1_MISSING 255
#define PARASOL_LEVEL3_UI2_MISSING 65535
#define PARASOL_LEVEL3_UI1_OUT_OF_RANGE 254
#define PARASOL_LEVEL3_UI2_OUT_OF_RANGE 65534

#define POLDER_LEV1_LEV2_NODE_LON_OFFSET 590
#define POLDER_LEV1_LEV2_NODE_DATE_OFFSET 598
#define POLDER_LEV1_LEV2_FIRST_DATE_OFFSET 640
#define POLDER_LEV1_LEV2_LAST_DATE_OFFSET 656
#define POLDER_LEV3_SYNTH_FIRST_DATE_OFFSET 700
#define POLDER_LEV3_SYNTH_LAST_DATE_OFFSET 716
#define POLDER_LEV3_SYNTH_REF_DATE_OFFSET 732

#define OUTPUT_FORMAT_MAXLEN 32

#define DATE_LEN 16 /* length of dates in POLDER/PARASOL leader files (strings of 16 characters without NULL ending character) */
#define NODE_LON_LEN 8 /* length of node longitudes in POLDER/PARASOL leader files (strings of 8 characters terminated by a space) */

#define DEFAULT_TIME_FORMAT "%H:%M:%S"
#define DEFAULT_DATE_FORMAT "%F"

#define ROWCOL_UNDEFINED -1
#define LATLON_UNDEFINED -999.

#define NORTHERN_PIXEL_ROW_OFFSET 840
#define SOUTHERN_PIXEL_ROW_OFFSET 844

#ifndef APPNAME
# define APPNAME "parasolascii"
#endif

/* a portable version of GNU timgm, from the man page of timegm */
time_t my_timegm (struct tm *tm) {
  time_t ret;
  char *tz;

  tz = getenv("TZ");
  setenv("TZ", "UTC", 1);
  tzset();
  ret = mktime(tm);
  if (tz)
    setenv("TZ", tz, 1);
  else
    unsetenv("TZ");
  tzset();
  return ret;
}

extern long int lround(double x); /* not available with gcc unless -ansi is set (don't know why) */

static char param_file[MAX_STRING_LENGTH + 1];
static char param_names[NMAX_PARAMS][MAX_STRING_LENGTH + 1];
static char param_comments[NMAX_PARAMS][MAX_STRING_LENGTH + 1];
static int param_positions[NMAX_PARAMS];
static int param_sizes[NMAX_PARAMS];
static int param_indexes[NMAX_PARAMS];
static int max_param;

/* affiche un message en cas de mauvaise utilisation de la commande */
void usage() {
  fprintf(stderr,"\n%s version %s\n\n", APPNAME, version);
  fprintf(stderr,"usage : %s <POLDER/PARASOL_data_file> parameter_1 [parameter_2 ...]\n\n", APPNAME);
  exit(1);
}

/* initialise les tableaux de parametres a partir d'un fichier de donnees "param_file" */
void init_param(const char *param_file) {
  FILE *fp;
  int nread;
  char param_name[MAX_STRING_LENGTH + 1], param_comment[MAX_STRING_LENGTH + 1];
  int param_index, param_pos, param_size;
  char first_char; /* premier caractere d'une ligne du fichier param_file (utilise pour les lignes de commentaire) */
  char skip[MAX_STRING_LENGTH + 1];
  unsigned int line;
  unsigned int i;
	
  fp = fopen(param_file, "r");
  if (fp == NULL) {
    perror(param_file);
    exit(4);
  }
	
  line = 1;
  i = 0;
	
  do {
    first_char = fgetc(fp);
    ungetc(first_char, fp);
    if (first_char == EOF) break ;
    else if (first_char == '#') { /* elimine les lignes de commentaire */
      fgets(skip, MAX_STRING_LENGTH, fp);
      if (skip == NULL) {
	perror(param_file);
	exit(5);
      }
    } /* first_char == '#' */
    else {
      nread = fscanf(fp, "%s %i %i %i", &param_name[0], &param_pos, &param_index, &param_size);
      if (nread != 4) {
	if (feof(fp)) break;
	fprintf(stderr, "%s : %s : unexpected format by line %d (4 columns expected + comment)\n", APPNAME, param_file, line);
	exit(6);
      }
			
      fgets(param_comment, MAX_STRING_LENGTH, fp);
			
      strcpy(param_names[i],param_name);
      param_positions[i] = param_pos;
      param_indexes[i] = param_index;
      param_sizes[i] = param_size;
      strcpy(param_comments[i++], param_comment);
    }
    line++;
  } while (! feof(fp));
	
  max_param = i - 1;
  fclose(fp);
}

/* affiche la liste des parametres existants, et un commentaire d'explication sur leur
 * signification
 */
void list_param() {
  int i;
	
  for (i=0 ; i<=max_param ; i++) {
    if (param_names[i][0] != '\0') printf("%-30.30s%s",param_names[i],param_comments[i]);
  }

}



/* pour un parametre de nom "param_name" en entree, renvoie :
 * param_ind : indice du parametre
 * param_pos : position du parametre (en octets relatifs par rapport au debut de l'enregistrement
 *   des donnees)
 * param_size : la taille du parametre en octets
 *
 * l'indice de parametre est egalement retourne comme valeur de get_param
 * (-1 en cas d'echec)
 */
int get_param(const char *param_name, int *param_ind, int *param_pos, int *param_size) {
  int i;
  char param_prefix[MAX_STRING_LENGTH + 1];
	
  memset(param_prefix, 0, sizeof(param_prefix));
	
  for (i=0 ; i<=max_param ; i++) {
    if (strcmp(param_name, param_names[i]) == 0) {
      *param_ind = param_indexes[i];
      *param_pos = param_positions[i];
      *param_size = param_sizes[i];
      return i;
    }
  }
  return -1;
}

int main(int argc, char *argv[]) {

  parasol_leader_scaling_factors_t parasol_leader_scaling_factors;
  int last_char_index;
  char param_name[NMAX_PARAMS][MAX_STRING_LENGTH + 1];
  int param_ind[NMAX_PARAMS]; 
  int param_pos[NMAX_PARAMS];
  int param_size[NMAX_PARAMS];
  unsigned long scaling_factors_offset;
  unsigned long leader_annotation_offset;
  unsigned char resolution;
  unsigned char parasol_level;
  float parameter_offset[NMAX_PARAMS], parameter_slope[NMAX_PARAMS];
  char leader_file[MAX_STRING_LENGTH + 1], data_file[MAX_STRING_LENGTH + 1];
	
  int option_flags ; /* drapeau d'options */
  int k;
  
  FILE *leader_fp ; /* Leader file pointer */

  /* pour la determination de file_type */
  char basec[MAX_STRING_LENGTH + 1];
  char *bname;
  char file_type[9]; /* P[1-3]L1TBG1, P[1-3]L1TBGC, P[1-3]L2TRGB, P[1-3]L2TOGB, P[1-3]L2TOGC, P[1-3]L2TOGV, 
		      * P[1-3]L2TLGA, P[1-3]L2TLGC, P[1-3]L2TLGV, P[1-3]L3TLGC, P[1-3]L3TOGC, P[1-3]L3TLGA, 
		      * P[1-3]L3TRGB 
		      */
  char polder_version; /* defined by file_type[1] */
   
  if (argc < 2) usage();

  /* verifier que l'on donne bien un fichier leader (termine par L) ou data (termine par D) en argument
     supprimer le D ou le L terminal */
  last_char_index = strlen(argv[1])-1;
  if ((argv[1][last_char_index] == 'D') || (argv[1][last_char_index] == 'L'))
    argv[1][last_char_index] = '\0';
  else {
    fprintf(stderr, "%s : invalid product name (must be suffixed by D or L)\n", argv[1]);
    exit (EXIT_FAILURE);
  }

  /* reformer les noms de fichiers leader et data a partir de chaque argument */
  sprintf(leader_file, "%sL", argv[1]);
  sprintf(data_file, "%sD", argv[1]);

  /* determination du type de fichier */
  memset(file_type, 0, sizeof(file_type));
  strncpy(basec, data_file, sizeof(basec) - 1);
  bname = basename(basec);
  strncpy(file_type, &bname[0], 8);
  polder_version = file_type[1];
  if (('1' <= polder_version && polder_version <= '3') == FALSE) {
    fprintf(stderr, "%s: this version of %s supports only POLDER-1, POLDER-2, and POLDER-3 (PARASOL), not POLDER-%c\n",
	    APPNAME, APPNAME, polder_version);
    exit (EXIT_FAILURE);
  }
  sprintf(param_file, "%s/PARAM_%s.txt", PARAM_PATH, file_type);

  /* verifier le type de produit pour preparer son decodage */
  if (strstr(file_type, "L2TRGB") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
  } else if (strstr(file_type, "L2TOGB") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
  } else if (strstr(file_type, "L2TOGC") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
  } else if (strstr(file_type, "L2TOGF") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
  } else if (strstr(file_type, "L2TOGV") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
  } else if (strstr(file_type, "L2TOGW") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
  } else if (strstr(file_type, "L2TLGC") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
  } else if (strstr(file_type, "L2TLGV") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
  } else if (strstr(file_type, "L2TLGA") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
  } else if (strstr(file_type, "L1TBG1") != NULL) {
    scaling_factors_offset = LEVEL1_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL1_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL1;
  } else if (strstr(file_type, "L1TBGC") != NULL) {
    scaling_factors_offset = LEVEL1_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL1_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL1;
  } else if (strstr(file_type, "L3TLGC") != NULL) {
    scaling_factors_offset = LEVEL3_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL3_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL3;	
  } else if (strstr(file_type, "L3TOGC") != NULL) {
    scaling_factors_offset = LEVEL3_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL3_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL3;	
  } else if (strstr(file_type, "L3TLGA") != NULL) {
    scaling_factors_offset = LEVEL3_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL3_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL3;
  } else if (strstr(file_type, "L3TLGB") != NULL) {
    scaling_factors_offset = LEVEL3_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL3_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL3;		
  } else if (strstr(file_type, "L3TRGB") != NULL) {
    scaling_factors_offset = LEVEL3_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL3_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL3;
  } else {
    fprintf(stderr, "%s : unknown or unimplemented product (available today: L1TBG1, L1TBGC, L2TRGB, L2TOGB, L2TOGC, L2TOGV, L2TLGA, L2TLGC, L2TLGV, L3TLGC, L3TOGC, L3TLGA, L3TRGB)\n", argv[1]);
    exit (EXIT_FAILURE);
  }    
  
  init_param(param_file);
		
  /* Ouverture du fichier leader */
  leader_fp = fopen((const char *) leader_file, "rb");
  if (leader_fp == NULL) {
    perror(leader_file);
    exit(2);
  }

  for (k = 0 ; k <= max_param ; k++) {
	
    strcpy(param_name[k], param_names[k]);
	
    if (get_param(param_name[k], &param_ind[k], &param_pos[k], &param_size[k]) == -1) {
      fprintf(stderr,"%s : unknown parameter.\nPlease check parameters from file %s\n",param_name[k], param_file);
      exit(7);
    }
    
    memset(&parasol_leader_scaling_factors, (char) 0, sizeof(parasol_leader_scaling_factors));
    
    fseek(leader_fp, scaling_factors_offset, SEEK_SET);
    fread(&parasol_leader_scaling_factors.record_number, sizeof(int), 1, leader_fp);
    fread(&parasol_leader_scaling_factors.record_length, sizeof(int), 1, leader_fp);
    fread(&parasol_leader_scaling_factors.interleaving, 8*sizeof(char), 1, leader_fp);
    fread(&parasol_leader_scaling_factors.byte_ordering, 16*sizeof(char), 1, leader_fp);
    fread(&parasol_leader_scaling_factors.npar, 4*sizeof(char), 1, leader_fp);
    fread(&parasol_leader_scaling_factors.nbytes, 8*sizeof(char), 1, leader_fp);
	
    parasol_leader_scaling_factors.record_number = ntohl(parasol_leader_scaling_factors.record_number);
    parasol_leader_scaling_factors.record_length = ntohl(parasol_leader_scaling_factors.record_length);
	
    fseek(leader_fp, scaling_factors_offset + 26*param_ind[k] + 20, SEEK_SET);
				
    fread(&parasol_leader_scaling_factors.slope, 12*sizeof(char), 1, leader_fp);
    fread(&parasol_leader_scaling_factors.offset, 12*sizeof(char), 1, leader_fp);
	
    parameter_slope[k] = (float) atof(parasol_leader_scaling_factors.slope);
    parameter_offset[k] = (float) atof(parasol_leader_scaling_factors.offset);	

    fprintf(stdout, "%s\t%f\t%f\n", param_name[k], parameter_slope[k], parameter_offset[k]);

    /*
    if (option_flags & DEBUG_MODE) {
      fprintf(stderr, "%s : (%i) : %i : param_name : %s  param_ind : %i  "
	      "param_pos : %i  param_size : %i\n",APPNAME,
	      __LINE__, k, param_name[k], param_ind[k], param_pos[k], param_size[k]);
      fprintf(stderr, "%s : (%i) : slope : %f  offset : %f\n",APPNAME,
	      __LINE__, parameter_slope[k], parameter_offset[k]);
    }
    */
		
  } /* for k */
	
  fclose(leader_fp); /* fermeture du leader file */
	
  if (option_flags & DEBUG_MODE) exit(EXIT_FAILURE);

  return EXIT_SUCCESS;
	
} /* main */
