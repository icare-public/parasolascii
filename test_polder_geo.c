/* test_polder_geo.c */
#include <stdlib.h>
#include <stdio.h>
#include "polder_geo.h"

int main(int argc, char *argv[]) {
  const char *arg_convert_mode;
  const char *arg_resolution;
  double coord1_in, coord2_in;
  double coord1_out, coord2_out;
  double lat, lon;
  double row, col;
  int res;
  int err;

  if (argc != 5) {
    fprintf(stderr, "%s <ll2rc|rc2ll> <res> <coord1> <coord2>\n\n", argv[0]);
    fprintf(stderr, "ll2rc: converts lat,lon to row,col\n");
    fprintf(stderr, "rc2ll: converts row,col to lat,lon\n");
    fprintf(stderr, "res:   resolution (F,M,L)\n");
    exit (EXIT_FAILURE);
  }

  arg_convert_mode = argv[1];
  arg_resolution   = argv[2];
  coord1_in        = atof(argv[3]);
  coord2_in        = atof(argv[4]);

  if (arg_resolution[0] == 'F') {
    res = POLDER_FULL_RESOLUTION;
  }
  else if (arg_resolution[0] == 'M') {
    res = POLDER_MEDIUM_RESOLUTION;
  }
  else if (arg_resolution[0] == 'L') {
    res = POLDER_LOW_RESOLUTION;
  }
  else {
    fprintf(stderr, "%s: bad second argument: %s (should be F,M,L)\n", argv[0], arg_resolution);
    exit (EXIT_FAILURE);
  }

  if (strcmp(arg_convert_mode, "ll2rc") == 0) {
    lat = coord1_in;
    lon = coord2_in;
    err = latlon2ligcol_polder(lat, lon, res, &row, &col);
    coord1_out = row;
    coord2_out = col;
  }
  else if (strcmp(arg_convert_mode, "rc2ll") == 0) {
    row = coord1_in;
    col = coord2_in;
    err = ligcol2latlon_polder(row, col, res, &lat, &lon);
    coord1_out = lat;
    coord2_out = lon;
  }
  else {
    fprintf(stderr, "%s: bad first argument: %s (should be ll2rc or rc2ll)\n", argv[0], arg_convert_mode);
    exit (EXIT_FAILURE);
  }
  
  if (err == 0) {
    fprintf(stdout, "%s: %f %f --> %f %f\n", argv[0], coord1_in, coord2_in, coord1_out, coord2_out);
  }
  else {
    fprintf(stderr, "%s: %f %f: bad input for conversion\n", argv[0], coord1_in, coord2_in);
  }
  return EXIT_SUCCESS;

}
