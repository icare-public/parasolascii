#!/bin/sh

if [ -z "$PROGRAMFILES" ] ; then
    echo "$0: you seem not to be on a Windows/Cygwin system. If you're on a native Unix-like system, run install.sh instead" 1>&2
    exit 1
fi

choice_1="$PROGRAMFILES/parasolascii"
choice_2="$HOMEPATH/parasolascii"

cat << END_OF_CAT
Where do you wish parasolascii to be installed ?
  0) abort installation
  1) $choice_1
  2) $choice_2
  3) a directory of your choice
END_OF_CAT

echo -n "your choice: "
read choice

case $choice in
    0) exit 0 ;;
    1) INSTALL_DIR=$choice_1 ;;
    2) INSTALL_DIR=$choice_2 ;;
    3) echo -n "which directory: "
       read INSTALL_DIR
       ;;
    *) echo "bad choice (0, 1, 2 or 3 only)" 1>&2
       exit 1 ;;
esac

echo -n "CAUTIOUS: the directory [$INSTALL_DIR] will be overwritten if it already exists, are you sure ? [type yes to confirm]: "
read confirmation
if [ "$confirmation" != 'yes' ] ; then
    echo 'installation aborted' 1>&2
    exit 1
fi
export INSTALL_DIR

make clean
make

[ -d "$INSTALL_DIR" ] && rm -rf "$INSTALL_DIR"

cp -r "$PWD" "$INSTALL_DIR"

echo "add $INSTALL_DIR to your PATH environment variable so you can use the command from anywhere"
echo "  PATH=%PATH%;$INSTALL_DIR"

echo
echo "NOTE: if you need to deploy the binary executable elsewhere, don't forget to add cygwin1.dll to your library path"
exit 0
