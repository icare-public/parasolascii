/* map2ascii
 *
 * affiche en colonnes (superpixel par superpixel ou pixel par pixel)
 * les valeurs des produits de niveau 2
 * stockees dans les fichiers images donnes en argument
 *
 * Le resultat peut etre redirige vers un fichier a passer
 * a un logiciel de trace comme Mgraph ou gnuplot
 *
 *
 * Exemple : polprint SPX_NON_DIR_A1_865.grid1x1 SPX_NON_DIR_A2_865.grid1x1
 * renvoie sur la sortie standard les colonnes des produits A1_865 et A2_865
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "unistd.h"

#define NB_LIGNES_1x1 3240
#define NB_COLONNES_1x1 6480
#define NB_LIGNES_3x3 1080
#define NB_COLONNES_3x3 2160
#define NB_LIGNES_9x9 360
#define NB_COLONNES_9x9 720
#define DEFINED 0
#define UNDEF_F -999.
#define UNDEF_L 0xFFFFFFFF
#define UNDEF_S 0xFFFF
#define UNDEF_C 0xFF
#define UNDEF -1
#define DISABLED 0
#define ENABLED 1
#define COORD_CARTESIENNES 1
#define COORD_GEOGRAPHIQUES 2
#define TYPE_CHAR 0
#define TYPE_SHORT 1
#define TYPE_LONG 2
#define TYPE_FLOAT 3

#define FALSE 0
#define TRUE 1

const int Nb[] = {
  3,   9,  15,  21,  27,  36,  45,  45,  54,  63,  64,  80,  80,  80,  96,  88, 110, 110, 110, 132,
  140, 140, 140, 140, 140, 170, 170, 170, 170, 170, 200, 200, 200, 200, 200, 230, 230, 230, 230, 230,
  260, 260, 260, 260, 260, 290, 290, 290, 290, 290, 320, 320, 320, 320, 320, 345, 345, 345, 345, 345,
  375, 375, 375, 375, 375, 400, 400, 400, 400, 400, 425, 425, 425, 425, 425, 450, 450, 450, 450, 450,
  475, 475, 475, 475, 475, 500, 500, 500, 500, 500, 520, 520, 520, 520, 520, 540, 540, 540, 540, 540,
  560, 560, 560, 560, 560, 580, 580, 580, 580, 580, 600, 600, 600, 600, 600, 615, 615, 615, 615, 615,
  630, 630, 630, 630, 630, 645, 645, 645, 645, 645, 660, 660, 660, 660, 660, 670, 670, 670, 670, 670,
  680, 680, 680, 680, 680, 690, 690, 690, 690, 690, 700, 700, 700, 700, 700, 705, 705, 705, 705, 705,
  710, 710, 710, 710, 710, 715, 715, 715, 715, 715, 720, 720, 720, 720, 720, 720, 720, 720, 720, 720,
  720, 720, 720, 720, 720, 720, 720, 720, 720, 720, 715, 715, 715, 715, 715, 710, 710, 710, 710, 710,
  705, 705, 705, 705, 705, 700, 700, 700, 700, 700, 690, 690, 690, 690, 690, 680, 680, 680, 680, 680,
  670, 670, 670, 670, 670, 660, 660, 660, 660, 660, 645, 645, 645, 645, 645, 630, 630, 630, 630, 630,
  615, 615, 615, 615, 615, 600, 600, 600, 600, 600, 580, 580, 580, 580, 580, 560, 560, 560, 560, 560,
  540, 540, 540, 540, 540, 520, 520, 520, 520, 520, 500, 500, 500, 500, 500, 475, 475, 475, 475, 475, 
  450, 450, 450, 450, 450, 425, 425, 425, 425, 425, 400, 400, 400, 400, 400, 375, 375, 375, 375, 375, 
  345, 345, 345, 345, 345, 320, 320, 320, 320, 320, 290, 290, 290, 290, 290, 260, 260, 260, 260, 260,
  230, 230, 230, 230, 230, 200, 200, 200, 200, 200, 170, 170, 170, 170, 170, 140, 140, 140, 140, 140, 
  132, 110, 110, 110,  88,  96,  80,  80,  80,  64,  63,  54,  45,  45,  36,  27,  21,  15,   9,   3 
};

int filesize(char* filename);

int main(int argc, char* argv[]) {

  int i,j,k;
  int imin = 0, imax = 0, jmin = 0, jmax = 0;
  int colonnes_definies;
  int nb_images;
  int NB_LIGNES, NB_COLONNES;
  FILE* clonehandle[255];
  float donnee_f[255];
  unsigned long  donnee_l[255];
  unsigned short donnee_s[255];
  unsigned char  donnee_c[255];
  float* images; /* tableau des fichiers images a lire */
  char *pos_comma, minarg[255], maxarg[255];
  char* maskfile = NULL;
  FILE* maskhandle = NULL;
  float maskvalue;
  int option;
  char coordinates = DISABLED;
  char* liste_options = "cguUm:L:C:I:J:t:139";
  int data_type = TYPE_FLOAT;
  double lat,lon;
  int opt_u = FALSE;
  int opt_U = FALSE;
  const float zero = 0. ;
  const float NaN = zero / zero ;
  
  if (argc == 1) {
    printf("\n_____________________________________________\n");
    printf("\n                   map2ascii                 \n");
    printf("_____________________________________________\n\n");
    printf("\nusage : map2ascii [-c] [-m <fichier_de_masque>] image_polder1 [image_polder2 image_polder3...]\n\n");
    printf("affiche les donnees binaires contenues dans les fichiers d'images Polder sous forme de colonnes ascii\n");
    printf("ordonnees dans l'ordre specifie sur la ligne de commande.\n\n");
    printf("par defaut, le programme affiche seulement les lignes dont toutes les colonnes sont definies\n");
    printf("\n\noptions :\n");
    printf("-c : affiche les coordonnees cartesiennes (ligne, colonne) sur les 2 premieres colonnes\n");
    printf("-g : affiche les coordonnees geographiques (latitude, longitude) sur les 2 premieres colonnes\n");
    printf("-u : affiche les lignes dont au moins une colonne est definie, les indefinis sont affiches comme des NaN\n");
    printf("-U : affiche les lignes dont au moins une colonne est definie, les indefinis ne sont pas affiches\n");
    printf("-m <fichier *.mask> : filtre les donnees specifiees par le fichier de masque\n"
	   "     (fichier de meme taille que l'image, de type char et valant 0 sur les valeurs a masquer,\n"
	   "     tout pixel de masque non nul etant preserve)\n");
    printf("-t : type de valeur (char,short,long,float) (a ne pas utiliser pour les fichiers clone)\n");
    printf("-1 : lecture en pleine resolution (1x1)\n");
    printf("-3 : lecture en moyenne resolution (3x3)\n");
    printf("-9 : lecture en basse resolution (9x9)\n");
    printf("-L <nb de lignes> : specifie le nombre de lignes du/des fichier(s) a lire\n");
    printf("-C <nb de colonnes> : specifie le nombre de colonnes du/des fichier(s) a lire\n");
    printf("-I ligne_min,ligne_max : specifie l'intervalle de lignes utiles a parcourir\n");
    printf("-J col_min,col_max : specifie l'intervalle de colonnes utiles a parcourir\n");
    printf("\n");
    exit(0);
  }
  
  while ((option = getopt(argc, argv, liste_options)) != -1) {
    switch(option) {
    case 'c' :
      coordinates = COORD_CARTESIENNES;
      break;
    case 'g' :
      coordinates = COORD_GEOGRAPHIQUES;
      break;
    case 'u' :
      opt_u = TRUE ;
      break;
    case 'U' :
      opt_U = TRUE ;
      break;
    case 'm' :
      maskfile = optarg;
      break;
    case 'I' :
      if ((pos_comma = strchr(optarg, ',')) == NULL) {
	fprintf(stderr, "map2ascii : argument de l'option -I invalide : %s\n", optarg);
	exit(EXIT_FAILURE);
      }
      strncpy(minarg, optarg, pos_comma-optarg);
      strncpy(maxarg, pos_comma + sizeof(char), (optarg+strlen(optarg)-pos_comma));
      imin = atoi(minarg);
      imax = atoi(maxarg);
      break;
    case 'J' :
      if ((pos_comma = strchr(optarg, ',')) == NULL) {
	fprintf(stderr, "map2ascii : argument de l'option -J invalide : %s\n", optarg);
	exit(EXIT_FAILURE);
      }
      strncpy(minarg, optarg, pos_comma-optarg);
      strncpy(maxarg, pos_comma + sizeof(char), (optarg+strlen(optarg)-pos_comma));
      jmin = atoi(minarg);
      jmax = atoi(maxarg);
      break;
    case 'L' :
      NB_LIGNES = atoi(optarg);
      break;
    case 'C' :
      NB_COLONNES = atoi(optarg);
      break;
    case '1' :
      NB_LIGNES = NB_LIGNES_1x1;
      NB_COLONNES = NB_COLONNES_1x1;
      break;
    case '3' :
      NB_LIGNES = NB_LIGNES_3x3;
      NB_COLONNES = NB_COLONNES_3x3;
      break;
    case '9' :
      NB_LIGNES = NB_LIGNES_9x9;
      NB_COLONNES = NB_COLONNES_9x9;
      break;
    case 't' :
      if (strcmp((const char*) optarg,"char") == 0) data_type = TYPE_CHAR;
      else if (strcmp((const char*) optarg,"short") == 0) data_type = TYPE_SHORT;
      else if (strcmp((const char*) optarg,"long") == 0) data_type = TYPE_LONG;
      else if (strcmp((const char*) optarg,"float") == 0) data_type = TYPE_FLOAT;
      else {
	fprintf(stderr, "erreur : argument de l'option -t invalide (char, short, long ou float uniquement)\n");
	exit(1);
      }
      break;
    case '?' :
      printf("option %c inconnue\n", optopt);
      exit(1);
    }
  } 

  nb_images = argc - optind;

  if (NB_LIGNES == 0 || NB_COLONNES == 0) {

    /* analyse du premier fichier pour determiner la taille de la grille a traiter */
 
    if (filesize(argv[optind+0]) == NB_LIGNES_3x3*NB_COLONNES_3x3*sizeof(float)) {
      NB_LIGNES = NB_LIGNES_3x3;
      NB_COLONNES = NB_COLONNES_3x3;
    }
    else if (filesize(argv[optind+0]) == NB_LIGNES_1x1*NB_COLONNES_1x1*sizeof(float)) {
      NB_LIGNES = NB_LIGNES_1x1;
      NB_COLONNES = NB_COLONNES_1x1;
    }
    else if (     (data_type == TYPE_FLOAT && 
		   filesize(argv[optind+0]) == NB_LIGNES_9x9*NB_COLONNES_9x9*sizeof(float))
		  ||  (data_type == TYPE_LONG && 
		       filesize(argv[optind+0]) == NB_LIGNES_9x9*NB_COLONNES_9x9*sizeof(long))
		  ||  (data_type == TYPE_SHORT && 
		       filesize(argv[optind+0]) == NB_LIGNES_9x9*NB_COLONNES_9x9*sizeof(short))
		  ||  (data_type == TYPE_CHAR && 
		       filesize(argv[optind+0]) == NB_LIGNES_9x9*NB_COLONNES_9x9*sizeof(char))	
		  ) {
      NB_LIGNES = NB_LIGNES_9x9;
      NB_COLONNES = NB_COLONNES_9x9;
    }
    else {
      fprintf(stderr,"Le(s) fichier(s) a traiter sont incompatibles avec les dimensions specifiees.\n");
      exit(1);
    }
	
  } /* (NB_LIGNES == 0 || NB_COLONNES == 0) */

  /* verification de l'identite de taille pour tous les fichiers */
  for (i=0; i< nb_images ; i++) {
    if (filesize(argv[optind+i]) != filesize(argv[optind+0])) {
      fprintf(stderr,"Les fichiers a traiter n'ont pas tous la meme taille !\n");
      exit(1);
    }
	
    if ((clonehandle[i] = fopen(argv[optind+i],"r")) == NULL) {
      perror(argv[optind+i]);
      exit(1);
    }
  }
 
  
  /* analyse du fichier de masque s'il existe */
  if (maskfile) {
    if (filesize(maskfile) != NB_LIGNES*NB_COLONNES*sizeof(char)) {
      fprintf(stderr,"%s possede une taille incompatible avec les fichiers a traiter\n",maskfile);
      exit(1);
    }
    if ((maskhandle = fopen(maskfile,"r")) == NULL) {
      perror(maskfile);
      exit(1);
    }
  }
 
  if (! imin) imin=1;
  if (! imax) imax=NB_LIGNES;
  if (! jmin) jmin=1;
  if (! jmax) jmax=NB_COLONNES; 
  for (i=imin-1 ; i<imax ; i++) {
    for (j=jmin-1 ; j<jmax ; j++) {
      if (maskhandle != NULL) {
	fseek(maskhandle, (i*NB_COLONNES+j)*sizeof(char), SEEK_SET);
	fread(&maskvalue,sizeof(char),1,maskhandle);
	if (maskvalue == 0) continue;
      }
      colonnes_definies = nb_images;
      for (k=0 ; k<nb_images; k++) {
	if (data_type == TYPE_FLOAT) {
	  fseek(clonehandle[k],(i*NB_COLONNES+j)*sizeof(float), SEEK_SET);
	  fread(&donnee_f[k],sizeof(float),1,clonehandle[k]);
	}
	else if (data_type == TYPE_LONG) {
	  fseek(clonehandle[k],(i*NB_COLONNES+j)*sizeof(long), SEEK_SET);
	  fread(&donnee_l[k],sizeof(unsigned long),1,clonehandle[k]);
	}
	else if (data_type == TYPE_SHORT) {
	  fseek(clonehandle[k],(i*NB_COLONNES+j)*sizeof(short), SEEK_SET);
	  fread(&donnee_s[k],sizeof(unsigned short),1,clonehandle[k]);
	}
	else if (data_type == TYPE_CHAR) {
	  fseek(clonehandle[k],(i*NB_COLONNES+j)*sizeof(char), SEEK_SET);
	  fread(&donnee_c[k],sizeof(unsigned char),1,clonehandle[k]);
	}
			
	if (   (data_type == TYPE_FLOAT && donnee_f[k] == UNDEF_F)
	       || (data_type == TYPE_LONG && donnee_l[k] == (unsigned long) UNDEF_L)
	       || (data_type == TYPE_SHORT && donnee_s[k] == (unsigned short) UNDEF_S)
	       || (data_type == TYPE_CHAR && donnee_c[k] == (unsigned char) UNDEF_C)
	       ) { colonnes_definies-- ; 
	  if (data_type == TYPE_FLOAT) donnee_f[k] = NaN ;
	}
      }
      if ((colonnes_definies == 0) 
	  || (colonnes_definies < nb_images && (! opt_u) && (! opt_U))) { continue;}
		
      if (coordinates == COORD_CARTESIENNES) {
	printf("%d\t%d\t",i+1,j+1);
      }
      else if (coordinates == COORD_GEOGRAPHIQUES && NB_LIGNES == NB_LIGNES_3x3) {
	lat = 90.0 - 180.0*((i+1)-0.5)/NB_LIGNES_3x3;
	lon = 180.0*((j+1)-0.5-NB_LIGNES_3x3)/(NB_LIGNES_3x3*cos(lat*M_PI/180.0));
	printf("% 7.2f\t% 7.2f\t",lat,lon);
      }
      else if (coordinates == COORD_GEOGRAPHIQUES && NB_LIGNES == NB_LIGNES_9x9) {
	/* 
	   dans le manuel utilisateur polder level-2 p.29, 
	   on donne les formules de transformation suivantes :
	   lat = 90 - (iB-0.5)/2.0
	   lon = (jB-0.5)*360.0/Nb[iB]
	   avec 1 <= iB <= 360 et 1 <= jB <= 720
	   et Nb = { Nb[1], Nb[2], ..., Nb[360] }
			   
	   ici (iB-0.5) (resp. (jB-0.5)) est remplace par (i+0.5) (resp. (j+0.5))
	   car i = iB - 1 et j = jB - 1
	   Nb[iB] devient Nb[i] car le premier element de Nb, Nb[iB=1] dans le
	   manuel, est en fait Nb[i=0] en C.
			   
	*/
			
	lat = 90.0 - (i+0.5)/2.0; 
	lon = (j+0.5)*360.0/Nb[i];
	lon = fmod(lon, 360.0);
	if (lon >= 180.0) lon -= 360.0;
	printf("% 7.2f\t% 7.2f\t",lat,lon);
      }
      else if (coordinates == COORD_GEOGRAPHIQUES){
	printf("calcul des coordonnées géographiques seulement implémenté pour "
	       "les grilles de moyenne et basse résolution pour le moment, désolé !\n");
	return 1;
      }
      for (k=0 ; k<nb_images ; k++) {
	if (  ( (data_type == TYPE_FLOAT && isnan(donnee_f[k]))
		|| (data_type == TYPE_LONG && donnee_l[k] == (unsigned long) UNDEF_L)
		|| (data_type == TYPE_SHORT && donnee_s[k] == (unsigned short) UNDEF_S)
		|| (data_type == TYPE_CHAR && donnee_c[k] == (unsigned char) UNDEF_C)
		) && opt_U) continue;
	if (data_type == TYPE_FLOAT) printf("%.3f\t",(float) donnee_f[k]);
	else if (data_type == TYPE_LONG) printf("%ld\t",(long) donnee_l[k]);
	else if (data_type == TYPE_SHORT) printf("%d\t",(short) donnee_s[k]);
	else if (data_type == TYPE_CHAR) printf("%d\t",(char) donnee_c[k]);
      }
      printf("\n");
    } /* for j */
  } /* for i */
 
  if (maskhandle) fclose(maskhandle);
  for (i=0; i<nb_images ; i++) fclose(clonehandle[i]);
 
 
  return 0;

}


int filesize(char* filename) {
  FILE* filehandle;
  int _filesize;
	
  if ((filehandle = fopen((const char*) filename,"r")) == NULL) {
    perror(filename);
    exit(1);
  }
  fseek(filehandle, 0, SEEK_END);
  _filesize = ftell(filehandle);
  fclose(filehandle);
  return _filesize;

}
