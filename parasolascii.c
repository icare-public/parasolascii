/* 
Copyright or Copr. Fabrice Ducos, CGTD Icare, Laboratoire
d'Optique Atmospherique, 26 january 2015

<fabrice.ducos@univ-lille1.fr>

This software is a computer program whose purpose is to read
PARASOL data format.

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

/* I know that this code is terribly designed and has evoluted
 * in a bloated mess over years (at the time of this writing,
 * it has almost 10 years of existence). It does the "job" it was
 * intended for (provide a command line interface for Polder
 * products), but I can't say it's the code I'm most proud of.
 * It was at first developed for personal needs, as a quick home-made tool 
 * for validation purposes in my work and was found to be useful 
 * to some coworkers.
 * 
 * I've been planning for years to rewrite it completely (*)
 * but I have never managed to free enough time
 * to bring a version 2 to the same level of functionality.
 *
 * In the meantime, I hope that this code will be useful as is
 * to the Polder/Parasol satellite products.
 *
 * F. Ducos <fabrice.ducos@univ-lille1.fr>
 *
 * (*) what is lacking is a proper, up-to-date API for
 * Polder/Parasol products. One was developed for the CNES in Fortran and C
 * for the Polder-2 mission, but was delivered much later
 * after the first versions of this software, and unfortunately
 * lacks of the flexibility needed in this software. Moreover,
 * it has never been released to support all versions of Polder
 * (Polder-1, Polder-2 and Parasol) at the same time. 
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h> /* memset */
#include <unistd.h> /* getopt */
#include <limits.h>
#include <math.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
#include <libgen.h> /* basename */
#include "parasolascii.h"
#include "polder_geo.h"

#define FALSE 0
#define TRUE 1

#define min(a, b) ( (a) < (b) ? (a) : (b) )
#define max(a, b) ( (a) < (b) ? (b) : (a) )
#define MAX_STRING_LENGTH 1023
#define MAX_OUTPUT_LENGTH 65535
#define LEVEL1_SCALING_FACTORS_OFFSET 169380
#define LEVEL2_SCALING_FACTORS_OFFSET 3060
#define LEVEL3_SCALING_FACTORS_OFFSET 1260
#define DATA_RECORD_OFFSET 180

#define LEVEL1_LEADER_FILE_DESC_OFFSET 0
#define LEVEL2_LEADER_FILE_DESC_OFFSET 0
#define LEVEL3_LEADER_FILE_DESC_OFFSET 0

#define LEVEL1_LEADER_HEADER_OFFSET 180
#define LEVEL2_LEADER_HEADER_OFFSET 180
#define LEVEL3_LEADER_HEADER_OFFSET 180

/* no spatio-temporal parameters for LEVELS 3 */
#define LEVEL1_LEADER_SPATIOTEMP_OFFSET 540
#define LEVEL2_LEADER_SPATIOTEMP_OFFSET 540

/* no instrument settings for LEVELS 3 */
#define LEVEL1_LEADER_INSTR_SETTINGS_OFFSET 2160
#define LEVEL2_LEADER_INSTR_SETTINGS_OFFSET 2160

/* no technological parameters for LEVELS 2 and 3 */
#define LEVEL1_LEADER_TECHNO_PARAMS_OFFSET 2340

#define LEVEL1_LEADER_DATA_PROC_OFFSET 168660
#define LEVEL2_LEADER_DATA_PROC_OFFSET 2340
#define LEVEL3_LEADER_DATA_PROC_OFFSET 540

#define LEVEL1_LEADER_SCALING_FACTORS_OFFSET 169380
#define LEVEL2_LEADER_SCALING_FACTORS_OFFSET 3060
#define LEVEL3_LEADER_SCALING_FACTORS_OFFSET 1260

#define LEVEL1_LEADER_ANNOTATION_OFFSET 182520
#define LEVEL2_LEADER_ANNOTATION_OFFSET 16200
#define LEVEL3_LEADER_ANNOTATION_OFFSET 14400

#define NMAX_PARAMS 1024

#define N_ROWS_MEDIUMRES 1080
#define N_COLUMNS_MEDIUMRES 2160
#define N_ROWS_FULLRES 3240
#define N_COLUMNS_FULLRES 6480

#define RAW_MODE 2
#define DEBUG_MODE 4
#define COORD_PARASOL 8
#define COORD_PARASOL_PIX_AND_SPX 16
#define COORD_LATLON 32
#define COORD_REVERSED 64
#define COORD_TIME 128
#define COORD_NODE_LON 256
#define OUTPUT_MISSING 512
#define OUTPUT_OUT_OF_RANGE 1024
#define DQX_DECIMAL 2048
#define NEVER_DISCARD 4096

#define version "1.13.4"

#define MEDIUM_RESOLUTION 1
#define FULL_RESOLUTION 2

#define PARASOL_LEVEL1 1
#define PARASOL_LEVEL2 2
#define PARASOL_LEVEL3 3

#define PARASOL_LEVEL1_UI1_MISSING 0
#define PARASOL_LEVEL1_SI1_MISSING -127
#define PARASOL_LEVEL1_UI2_MISSING 0
#define PARASOL_LEVEL1_SI2_MISSING -32767
#define PARASOL_LEVEL1_SI2_OUT_OF_RANGE 32767
#define PARASOL_LEVEL2_UI1_MISSING 255
#define PARASOL_LEVEL2_UI2_MISSING 65535
#define PARASOL_LEVEL2_UI1_OUT_OF_RANGE 254
#define PARASOL_LEVEL2_UI2_OUT_OF_RANGE 65534
#define PARASOL_LEVEL3_UI1_MISSING 255
#define PARASOL_LEVEL3_UI2_MISSING 65535
#define PARASOL_LEVEL3_UI1_OUT_OF_RANGE 254
#define PARASOL_LEVEL3_UI2_OUT_OF_RANGE 65534

#define POLDER_LEV1_LEV2_NODE_LON_OFFSET 590
#define POLDER_LEV1_LEV2_NODE_DATE_OFFSET 598
#define POLDER_LEV1_LEV2_FIRST_DATE_OFFSET 640
#define POLDER_LEV1_LEV2_LAST_DATE_OFFSET 656
#define POLDER_LEV3_SYNTH_FIRST_DATE_OFFSET 700
#define POLDER_LEV3_SYNTH_LAST_DATE_OFFSET 716
#define POLDER_LEV3_SYNTH_REF_DATE_OFFSET 732

#define OUTPUT_FORMAT_MAXLEN 32

#define DATE_LEN 16 /* length of dates in POLDER/PARASOL leader files (strings of 16 characters without NULL ending character) */
#define NODE_LON_LEN 8 /* length of node longitudes in POLDER/PARASOL leader files (strings of 8 characters terminated by a space) */

#define DEFAULT_TIME_FORMAT "%H:%M:%S"
#define DEFAULT_DATE_FORMAT "%F"

#define ROWCOL_UNDEFINED -1
#define LATLON_UNDEFINED -999.

#define DEFAULT_MISSING_VALUE -999.

#define NORTHERN_PIXEL_ROW_OFFSET 840
#define SOUTHERN_PIXEL_ROW_OFFSET 844

#ifndef APPNAME
# define APPNAME "parasolascii"
#endif

#ifndef PARAM_PATH
# define PARAM_PATH "."
#endif

/* file types */
enum {
  FT_UNKNOWN,
  FT_L2TRGB,
  FT_L2TOGB,
  FT_L2TOGC,
  FT_L2TOGF,
  FT_L2TOGV,
  FT_L2TOGW,
  FT_L2TLGC,
  FT_L2TLGF,
  FT_L2TLGV,
  FT_L2TLGW,
  FT_L2TLGA,
  FT_L1TBG1,
  FT_L1TBGC,
  FT_L3TLGC,
  FT_L3TOGC,
  FT_L3TLGA,
  FT_L3TLGB,
  FT_L3TRGB
};

/* a portable version of GNU timgm, from the man page of timegm */
time_t my_timegm (struct tm *tm) {
  time_t ret;
  char *tz;

  tz = getenv("TZ");
  setenv("TZ", "UTC", 1);
  tzset();
  ret = mktime(tm);
  if (tz)
    setenv("TZ", tz, 1);
  else
    unsetenv("TZ");
  tzset();
  return ret;
}

extern long int lround(double x); /* not available with gcc unless -ansi is set (don't know why) */

static char param_file[MAX_STRING_LENGTH + 1];
static char param_names[NMAX_PARAMS][MAX_STRING_LENGTH + 1];
static char param_comments[NMAX_PARAMS][MAX_STRING_LENGTH + 1];
static int param_positions[NMAX_PARAMS];
static int param_sizes[NMAX_PARAMS];
static int param_indexes[NMAX_PARAMS];
static int max_param; 
static float missing;

static int nrows;
static int ncolumns;

static int min_row = ROWCOL_UNDEFINED;
static int min_column = ROWCOL_UNDEFINED;
static int max_row = ROWCOL_UNDEFINED;
static int max_column = ROWCOL_UNDEFINED;

static double min_lat = -90.;
static double min_lon = -180.;
static double max_lat = 90.;
static double max_lon = 180.;

static float out_of_range;
static unsigned short dqx_bit_to_print = 0;
static char FORMAT_DQX_ONE_BIT[] = "%1d\t";
static char FORMAT_DQX_ALL_BITS[] = "%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d\t";
static char *format_dqx = FORMAT_DQX_ALL_BITS;
static char output_format_temp[OUTPUT_FORMAT_MAXLEN + 1] = "%-9g";
static char output_format[OUTPUT_FORMAT_MAXLEN+1];
static char geographic_coord_format[OUTPUT_FORMAT_MAXLEN+1] = "% .3f    % .3f    ";
static char time_format[MAX_STRING_LENGTH] = "";

static int time_format_is_set = FALSE;
static int option_G_is_set = FALSE;

static int user_res = 0; /* resolution : 0 = guessed by parasolascii, 1 = medium (forced by the user), 2 = full (forced by the user) */

typedef enum err_type_{
  ERR_WARNING,
  ERR_FATAL
} err_type;

void print_sys_error(const char *file_name, err_type err) {
  fprintf(stderr, "%s: ", APPNAME);
  perror(file_name);
  if (err == ERR_FATAL) {
    exit(EXIT_FAILURE);
  }
}

#ifdef SHOW_WARNINGS
#warning "print_processing_version: not yet implemented"
#endif
void print_processing_version(int parasol_level, FILE *leader_fp) {
  int err;
  size_t nread;
  
  switch (parasol_level) {
  case PARASOL_LEVEL1:
    {
      assert(! "print_processing_version() not yet implemented for LEVEL 1\n");
      break;
    }
  case PARASOL_LEVEL2:
    {
      char processing_version[8 + 1];
      processing_version[8] = '\0';
      
      err = fseek(leader_fp, LEVEL2_LEADER_SCALING_FACTORS_OFFSET + 456, SEEK_SET);
      nread = fread(processing_version, sizeof(processing_version) - 1, 1, leader_fp);
      printf("processing_version: %s\n", processing_version);
    }
  case PARASOL_LEVEL3:
    {
      assert(! "print_processing_version() not yet implemented for LEVEL 3\n");
      break;
    }
  default : 
    {
      fprintf(stderr, "%s : internal bug : unexpected value for parasol_level : %d (contact the maintainer)\n", APPNAME, parasol_level);
      exit (EXIT_FAILURE);
      break;
    }
  } /* switch (parasol_level) */
}

/* affiche un message en cas de mauvaise utilisation de la commande */
void usage() {
  fprintf(stderr,"\n%s version %s\n\n", APPNAME, version);
  fprintf(stderr,"usage : %s <POLDER/PARASOL_data_file> parameter_1 [parameter_2 ...]\n\n", APPNAME);
  fprintf(stderr,"options\n");
  fprintf(stderr,"  -V            displays the version, and nothing else\n");
  fprintf(stderr,"  -c            displays the coordinates in the POLDER/PARASOL grid (row,column), in the fields (1,2)\n");
  fprintf(stderr,"  -S            displays the coordinates in the POLDER/PARASOL grid (row, column) of the current pixel in the fields (1,2)\n");
  fprintf(stderr,"                and the superpixel (S for superpixel) it belongs to in the fields (3,4). Will overload the -c option is present.\n");
  fprintf(stderr,"                Available for full resolution products only.\n");
  fprintf(stderr,"  -C <range>    displays the coordinates and data only in the suitable zone\n");
  fprintf(stderr,"                range = min_row[:min_column],max_row[:max_column] (for a zone)\n"); 
  fprintf(stderr,"                      = row:col (for exactly one pixel)\n");
  fprintf(stderr,"                (the -c option will be automatically enabled by -C)\n");
  fprintf(stderr,"  -g            displays the geographic coordinates (latitude, longitude), in the fields (1,2)\n");
  fprintf(stderr,"                if the options -c and -g are used altogether, the geographic coordinates\n");
  fprintf(stderr,"                will displayed in the fields (3,4)\n");
  fprintf(stderr,"  -G <range>    displays geographic coordinates and data only in the suitable zone\n");
  fprintf(stderr,"                range = min_lat[:min_lon],max_lat[:max_lon] (for a zone)\n");
  fprintf(stderr,"                      = lat:lon (for exactly one pixel)\n");
  fprintf(stderr,"                (the -g option will be automatically enabled by -G)\n");
  fprintf(stderr,"  -t            displays the acquisition time for each pixel\n");
  fprintf(stderr,"                CAUTIOUS: THIS OPTION IS (AND WILL STAY) EXPERIMENTAL, YOU SHOULD AVOID IT IF POSSIBLE\n");
  fprintf(stderr,"                Indeed, the time is not directly available in the products,\n");
  fprintf(stderr,"                it is currently estimated by linear interpolation on the rows between the first and\n");
  fprintf(stderr,"                last acquisition times. It is ONLY intended to be an estimate, since pixels contain data that are in fact\n");
  fprintf(stderr,"                directional and are not acquired at the same time. And the estimation method may be subject to changes.\n");
  fprintf(stderr,"                It can't be helped unless times are made available in the products.\n");
  fprintf(stderr,"                the -t option is valid only for LEVEL1 and LEVEL2 products\n");
  fprintf(stderr,"                unless the -T option is used, the default time format is \"%s\"\n", DEFAULT_TIME_FORMAT);
  fprintf(stderr,"  -d            displays the acquisition date for each pixel\n");
  fprintf(stderr,"                unless the -T option is used, the default date format is \"%s\"\n", DEFAULT_DATE_FORMAT);
  fprintf(stderr,"                with no parameter, the first date, reference date (node date for LEVEL1 and LEVEL2)\n");
  fprintf(stderr,"                and last date are displayed in this order\n");
  fprintf(stderr,"  -X            swaps (X for eXchanges) rows and columns and latitudes and longitudes in the output\n");
  fprintf(stderr,"                by default, %s displays rows before columns and latitudes before longitudes;\n", APPNAME);
  fprintf(stderr,"                some tools expect the reverse. This is what the -X option is for.\n");
  fprintf(stderr,"  -N            displays the node longitude (between -180 and +180), disabled for LEVEL3 products\n");
  /* the option -R is undocumented, it shouldn't be relied on, as it could be removed or modified at any time in the future */
  /*  fprintf(stderr,"  -R res        specifies the resolution of the product (res = f|m, f = full or m = medium)\n");
  fprintf(stderr,"                usually %s guesses the right resolution itself, but some products (namely OC2 and LS2) can exist\n", APPNAME);
  fprintf(stderr,"                in both resolutions (though the standard products are currently in medium resolution), and unfortunately this information\n");
  fprintf(stderr,"                is not kept in the file. So if you don't succeed to read a file with the usual resolution, you can try the other one.\n"); */
  fprintf(stderr,"  -T <time_fmt> sets the format used to display times and/or dates\n");
  fprintf(stderr,"                same syntax as the Unix date command, see the man page of date(1)\n");
  fprintf(stderr,"  -b            integer raw values (not scaled) from the POLDER/PARASOL data file\n");
  fprintf(stderr,"  -F <fmt>      output format for the parameter values (in printf-style), default is \"%s\"\n", output_format_temp);
  fprintf(stderr,"  -F g:<fmt>    output format for the geographic coordinates (in printf-style);\n");
  fprintf(stderr,"                default is \"%s\"\n", geographic_coord_format);
  fprintf(stderr,"                be aware that there must be two format specifiers, one for latitude and one for longitude\n");
  fprintf(stderr,"  -m <value>    missing values are displayed as <value>\n");
  fprintf(stderr,"  -r <value>    out of range values are displayed as <value>\n");
  fprintf(stderr,"  -u <value>    undefined or unknown values (i.e. missing or out of range) are displayed as <value>\n");
  fprintf(stderr,"                the -u flag is the same as -m and -r together, with the same value\n");
  fprintf(stderr,"                by default, missing values are displayed as %.0f\n", DEFAULT_MISSING_VALUE);
  fprintf(stderr,"                the special value 'discard' (without quotes) prevents to display output lines containing at least one missing value (1.9.x behavior)\n");
  fprintf(stderr,"                the special value 'never-discard' (without quotes) forces the tool to display also lines containing only missing values (available since 1.10.4)\n");
  fprintf(stderr,"                (by default lines with only missing values are discarded)\n");
  fprintf(stderr,"                CAVEAT: output lines with only missing values IS NOT THE SAME as missing records (output lines always correspond to available records)\n");
  fprintf(stderr,"  -h <P3L1TBG1|P3L2TRGB|...>  help on parameters (list of parameters for the specified thematic)\n");
  fprintf(stderr,"                (see below for the PARASOL naming scheme)\n");
  fprintf(stderr,"  -B <1-16>     (dqx bit to print - all bits in dqx are printed by default,\n");
  fprintf(stderr,"                the most significant at left. LSB=1, MSB=16)\n");
  fprintf(stderr,"  -x            displays dqx in decimal form, instead of binary form (level-1 files only)\n");
  fprintf(stderr,"  -D            debug mode (prints slope, offset and other debugging infos for each parameter)\n");
  fprintf(stderr,"  -P            displays the path to parameter files\n");
  /* -Q option for DQS decoding not implemented yet (and therefore not documented) */
  /* fprintf(stderr,"  -Q <flags>    extracts flags from the quality descriptor (dqs, level-2 files only)\n");
  fprintf(stderr,"                This is useful only if the user asks for the dqs parameter.\n");
  fprintf(stderr,"                The flags must be separated by commas (WITHOUT SPACES).\n");
  fprintf(stderr,"                The names of the flags are available in the parameter list (see the -h option)\n");
  fprintf(stderr,"                Example (for P3L2TOGC, i.e. aerosol on ocean products): -Q case-3,fixed-model\n");
  fprintf(stderr,"                The flags (0 or 1) will be displayed in the dqs column, separated by commas.\n");
  fprintf(stderr,"                By default, the dqs is displayed in decimal form.\n");
  */
  fprintf(stderr,"  -p <param_descriptor_file> reads the POLDER/PARASOL file using a given parameter file. Intended mainly for debugging purposes.\n");
  fprintf(stderr,"                The usage of this option is discouraged unless you do know what you're doing, and won't be documented further.\n");
  fprintf(stderr,"\n");
  fprintf(stderr,"POLDER/PARASOL files supported by this version :\n");
  fprintf(stderr,"  - P[1-3]L1TBG1 (radiances and Stokes parameters)\n");
  fprintf(stderr,"  - P3L1TBGC     (PARASOL radiances and Stokes parameters extracted under the CALIPSO's swath)\n");
  fprintf(stderr,"  - P[1-3]L2TRGB (clouds and radiative budget products)\n");
  fprintf(stderr,"  - P[1-2]L2TOGB (ocean color products)\n");
  fprintf(stderr,"  - P[1-3]L2TOGC (oceanic aerosol products)\n");
  fprintf(stderr,"  - P[1-3]L2TOGV (oceanic aerosol products, PARASOL validation files)\n");
  fprintf(stderr,"  - P[1-3]L2TLGC (continental aerosol products)\n");
  fprintf(stderr,"  - P[1-3]L2TLGV (continental aerosol products, PARASOL validation files)\n");
  fprintf(stderr,"  - P[1-3]L2TLGA (Land surface products)\n");
  fprintf(stderr,"  - P[1-3]L3TOGC (marine aerosol products L3)\n");
  fprintf(stderr,"  - P[1-3]L3TLGA (Land surface directional signature products L3)\n");
  fprintf(stderr,"  - P3L3TLGB     (Albedo and Vegetation parameters)\n");
  fprintf(stderr,"  - P[1-3]L3TLGC (continental aerosol products L3)\n");
  fprintf(stderr,"  - P[1-3]L3TRGB (clouds and radiative budget products L3)\n\n");
  fprintf(stderr,"For example :\n\n");
  fprintf(stderr,"To display the grid coordinates (-c), lat/lon coordinates (-g), cloud cover, cloud cover quality, and directional visible albedo (directions 1 and 2)\n");
  fprintf(stderr,"for the orbit P2L2TRGB044002BD (with -999. for undefined values, this is optional), please type :\n");
  fprintf(stderr,"%s -c -g -u -999. P2L2TRGB044002BD cc qcc avis_dir01 avis_dir02\n\n", APPNAME);
  
  fprintf(stderr, "From the version 1.10.1, wildcards are supported for directional parameters:\n");
  fprintf(stderr,"%s -c -g P3L1TBG1054111KD rad490P_dir* is a shorter and less tedious way to type:\n", APPNAME);
  fprintf(stderr,"%s -c -g P3L1TBG1054111KD rad490P_dir01 rad490P_dir02 ... rad490P_dir16\n\n", APPNAME);
  
  fprintf(stderr,"NB. THE LINES FOR WHICH ALL THE DATA ARE MISSING ARE DISCARDED (IT WOULD HAVE LITTLE SENSE TO DISPLAY HUGE VOLUME OF DATA\n");
  fprintf(stderr,"WITH ONLY MISSING VALUES). NEVERTHELESS THE LINES WITH AT LEAST ONE NON-MISSING VALUE WILL BE DISPLAYED. THIS IS THE\n");
  fprintf(stderr,"DEFAULT BEHAVIOUR SINCE VERSION 1.10.1\n");
  fprintf(stderr,"THE MISSING DATA ARE NOW DISPLAYED AS %.0f BY DEFAULT (THIS CAN STILL BE CHANGED WITH THE -u OPTION)\n", DEFAULT_MISSING_VALUE);
  fprintf(stderr,"TO DISCARD THE LINES AND RETRIEVE THE BEHAVIOUR OF VERSIONS 1.9.x, USE THE -u OPTION WITH THE KEYWORD discard : -u discard\n");
  fprintf(stderr,"\n");
  fprintf(stderr,"any question or bug report to Fabrice Ducos, fabrice.ducos@univ-lille1.fr\n\n");
  exit(1);
}

void init_param(const char *param_file) {
  FILE *fp;
  int nread;
  char param_name[MAX_STRING_LENGTH + 1], param_comment[MAX_STRING_LENGTH + 1];
  int param_index, param_pos, param_size;
  char first_char; /* premier caractere d'une ligne du fichier param_file (utilise pour les lignes de commentaire) */
  char skip[MAX_STRING_LENGTH + 1];
  unsigned int line;
  unsigned int i;
	
  fp = fopen(param_file, "r");
  if (fp == NULL) {
    perror(param_file);
    exit(4);
  }
	
  line = 1;
  i = 0;
	
  do {
    char *s;
    first_char = fgetc(fp);
    ungetc(first_char, fp);
    if (first_char == EOF) break ;
    else if (first_char == '#') { /* elimine les lignes de commentaire */
      s = fgets(skip, MAX_STRING_LENGTH, fp);
      if (s == NULL || skip == NULL) {
	perror(param_file);
	exit(5);
      }
    } /* first_char == '#' */
    else {
      nread = fscanf(fp, "%s %i %i %i", &param_name[0], &param_pos, &param_index, &param_size);
      if (nread != 4) {
	if (feof(fp)) break;
	fprintf(stderr, "%s : %s : unexpected format by line %d (4 columns expected + comment)\n", APPNAME, param_file, line);
	exit(6);
      }
			
      s = fgets(param_comment, MAX_STRING_LENGTH, fp);
      if (s == NULL || param_comment == NULL) {
	perror(param_file);
	exit(5);
      }
			
      strcpy(param_names[i],param_name);
      param_positions[i] = param_pos;
      param_indexes[i] = param_index;
      param_sizes[i] = param_size;
      strcpy(param_comments[i++], param_comment);
    }
    line++;
  } while (! feof(fp));
	
  max_param = i - 1;
  fclose(fp);
}

/* affiche la liste des parametres existants, et un commentaire d'explication sur leur
 * signification
 */
void list_param() {
  int i;
	
  for (i=0 ; i<=max_param ; i++) {
    if (param_names[i][0] != '\0') printf("%-30.30s%s",param_names[i],param_comments[i]);
  }

}

void set_param_file(char *param_file, const char *data_file /* for error messages */, FILE *data_fp, const char *str_file_type) {
  typedef struct Param_file_resolver {
    const char *str_file_type;
    size_t record_length; /* expected record length for the given file type */
    const char *param_file;
  } Param_file_resolver;
  size_t nread;
  int err;
  #define UNKNOWN 0

  Param_file_resolver param_file_resolver[] = {
    { "P1L1TBG1", 648,     PARAM_PATH "/PARAM_P1L1TBG1.txt" },
    { "P1L1TBGC", 648,     PARAM_PATH "/PARAM_P1L1TBGC.txt" },
    { "P1L2TRGB", 270,     PARAM_PATH "/PARAM_P1L2TRGB.until_v16.17.txt" },
    { "P1L2TRGB", 279,     PARAM_PATH "/PARAM_P1L2TRGB.from_v20.21.txt" },
    { "P1L2TOGB", UNKNOWN, PARAM_PATH "/PARAM_P1L2TOGB.txt" }, /* not yet implemented (record length unknown from lack of documentation) */
    { "P1L2TOGC",  50,     PARAM_PATH "/PARAM_P1L2TOGC.txt" },
    { "P1L2TOGV",1119,     PARAM_PATH "/PARAM_P1L2TOGV.txt" }, /* 13 (13 bytes for common fields) + 42 (42 bytes non_dir) + 14*76 (76 bytes per dir) */
    { "P1L2TLGC",  28,     PARAM_PATH "/PARAM_P1L2TLGC.without_aer_elv.txt" },
    { "P1L2TLGC",  32,     PARAM_PATH "/PARAM_P1L2TLGC.with_aer_elv.txt" },
    { "P1L2TLGV", 723,     PARAM_PATH "/PARAM_P1L2TLGV.txt" }, /* 10 (10 bytes for common fields) + 41 (41 bytes non_dir) + 14*48 (48 bytes per dir) */
    { "P1L2TLGA", 263,     PARAM_PATH "/PARAM_P1L2TLGA.txt" },
    { "P1L3TLGC",  83,     PARAM_PATH "/PARAM_P1L3TLGC.txt" },
    { "P1L3TOGC", 113,     PARAM_PATH "/PARAM_P1L3TOGC.txt" },
    { "P1L3TLGA",  90,     PARAM_PATH "/PARAM_P1L3TLGA.txt" },
    { "P1L3TLGB",  46,     PARAM_PATH "/PARAM_P1L3TLGB.txt" },
    { "P1L3TRGB",  84,     PARAM_PATH "/PARAM_P1L3TRGB.txt" },

    { "P2L1TBG1", 648,     PARAM_PATH "/PARAM_P2L1TBG1.txt" },
    { "P2L1TBGC", 648,     PARAM_PATH "/PARAM_P2L1TBGC.txt" },
    { "P2L2TRGB", 270,     PARAM_PATH "/PARAM_P2L2TRGB.until_v16.17.txt" },
    { "P2L2TRGB", 279,     PARAM_PATH "/PARAM_P2L2TRGB.from_v20.21.txt" },
    { "P2L2TOGB", UNKNOWN, PARAM_PATH "/PARAM_P2L2TOGB.txt" }, /* not yet implemented (record length unknown from lack of documentation) */
    { "P2L2TOGC",  50,     PARAM_PATH "/PARAM_P2L2TOGC.txt" },
    { "P2L2TOGV",1119,     PARAM_PATH "/PARAM_P2L2TOGV.txt" }, /* 13 (13 bytes for common fields) + 42 (42 bytes non_dir) + 14*76 (76 bytes per dir) */
    { "P2L2TLGC",  28,     PARAM_PATH "/PARAM_P2L2TLGC.without_aer_elv.txt" },
    { "P2L2TLGC",  32,     PARAM_PATH "/PARAM_P2L2TLGC.with_aer_elv.txt" },
    { "P2L2TLGV", 723,     PARAM_PATH "/PARAM_P2L2TLGV.txt" }, /* 10 (10 bytes for common fields) + 41 (41 bytes non_dir) + 14*48 (48 bytes per dir) */
    { "P2L2TLGA", 263,     PARAM_PATH "/PARAM_P2L2TLGA.txt" },
    { "P2L3TLGC",  83,     PARAM_PATH "/PARAM_P2L3TLGC.txt" },
    { "P2L3TOGC", 113,     PARAM_PATH "/PARAM_P2L3TOGC.txt" },
    { "P2L3TLGA",  90,     PARAM_PATH "/PARAM_P2L3TLGA.txt" },
    { "P2L3TLGB",  46,     PARAM_PATH "/PARAM_P2L3TLGB.txt" },
    { "P2L3TRGB",  84,     PARAM_PATH "/PARAM_P2L3TRGB.txt" },

    { "P3L1TBG1", 738,     PARAM_PATH "/PARAM_P3L1TBG1.txt" },
    { "P3L1TBGC", 738,     PARAM_PATH "/PARAM_P3L1TBGC.txt" },
    { "P3L2TRGB", 298,     PARAM_PATH "/PARAM_P3L2TRGB.until_v18.19.txt" },
    { "P3L2TRGB", 307,     PARAM_PATH "/PARAM_P3L2TRGB.from_v19.20.txt" },
    { "P3L2TOGB", UNKNOWN, PARAM_PATH "/PARAM_P3L2TOGB.txt" }, /* not yet implemented (record length unknown from lack of documentation) */
    { "P3L2TOGC",  50,     PARAM_PATH "/PARAM_P3L2TOGC.txt" },
    { "P3L2TOGF",  50,     PARAM_PATH "/PARAM_P3L2TOGF.txt" },
    { "P3L2TOGV",1271,     PARAM_PATH "/PARAM_P3L2TOGV.txt" }, /* 13 (13 bytes for common fields) + 42 (42 bytes non_dir) + 16*76 (76 bytes per dir) */
    { "P3L2TOGW",1271,     PARAM_PATH "/PARAM_P3L2TOGW.txt" },
    { "P3L2TLGC",  28,     PARAM_PATH "/PARAM_P3L2TLGC.without_aer_elv.txt" },
    { "P3L2TLGC",  32,     PARAM_PATH "/PARAM_P3L2TLGC.with_aer_elv.txt" },
    { "P3L2TLGF",  28,     PARAM_PATH "/PARAM_P3L2TLGC.without_aer_elv.txt" },
    { "P3L2TLGF",  32,     PARAM_PATH "/PARAM_P3L2TLGC.with_aer_elv.txt" },
    { "P3L2TLGV", 819,     PARAM_PATH "/PARAM_P3L2TLGV.txt" }, /* 10 (10 bytes for common fields) + 41 (41 bytes non_dir) + 16*48 (48 bytes per dir) */
    { "P3L2TLGW", 819,     PARAM_PATH "/PARAM_P3L2TLGV.txt" },
    { "P3L2TLGA", 297,     PARAM_PATH "/PARAM_P3L2TLGA.txt" },
    { "P3L3TLGC",  83,     PARAM_PATH "/PARAM_P3L3TLGC.txt" },
    { "P3L3TOGC", 113,     PARAM_PATH "/PARAM_P3L3TOGC.txt" },
    { "P3L3TLGA",  90,     PARAM_PATH "/PARAM_P3L3TLGA.txt" },
    { "P3L3TLGB",  46,     PARAM_PATH "/PARAM_P3L3TLGB.txt" },
    { "P3L3TRGB",  84,     PARAM_PATH "/PARAM_P3L3TRGB.txt" }
  };
  const size_t nentries = sizeof(param_file_resolver)/sizeof(*param_file_resolver);
  int i;

  int record_number;
  short short_record_length;
  size_t record_length;

  assert(param_file != NULL);
  assert(data_fp != NULL);
  assert(str_file_type != NULL);
  
  err = fseek(data_fp, DATA_RECORD_OFFSET, SEEK_SET); /* jumps the header record (180 bytes in the original format) */

  if (err == -1) { print_sys_error(param_file, ERR_FATAL); }
  nread = fread(&record_number, sizeof(int), 1, data_fp);
  if (feof(data_fp)) {
    fprintf(stderr, "%s: %s: no record found, file probably empty (except for the header)\n", APPNAME, data_file);
    exit (EXIT_FAILURE);
  }
  if (nread != 1) { print_sys_error(param_file, ERR_FATAL); }
  nread = fread(&short_record_length, sizeof(short), 1, data_fp);
  if (nread != 1) { print_sys_error(param_file, ERR_FATAL); }
  
  record_number = ntohl(record_number);
  record_length = (size_t) ntohs(short_record_length);

  for (i = 0 ; i < nentries ; i++) {
    size_t expected_record_length = param_file_resolver[i].record_length;

    if ((strcmp(str_file_type, param_file_resolver[i].str_file_type) == 0) 
	&& (record_length == expected_record_length)) {
	  strncpy(param_file, param_file_resolver[i].param_file, MAX_STRING_LENGTH);
	  return;
    }
  }
  
  fprintf(stderr, "%s: %s: bad record length (corrupted file, or bad version)\n", APPNAME, data_file); 
  exit (EXIT_FAILURE);
  
}



/* analyse les options passees en ligne de commande */
int parse_options(int *argc, char **argv[]) {
  int option;
  int flags;
	
  const char *options_list = "cxgdtDbB:C:G:h:u:r:m:NF:T:XPR:p:SV";
	
  flags = 0;
  
  missing = out_of_range = DEFAULT_MISSING_VALUE;
  flags |= OUTPUT_MISSING;
  flags |= OUTPUT_OUT_OF_RANGE;
	
  while ((option = getopt(*argc, *argv, options_list)) != -1 ) {
    switch(option) {
    case 'V' :
        {
            fprintf(stdout, "%s\n", version);
            exit(EXIT_SUCCESS);
        }
        break;
    case 'c' :
      flags |= COORD_PARASOL;
      break;
    case 'S' :
      flags |= COORD_PARASOL_PIX_AND_SPX;
      break;
    case 'C' :
      flags |= COORD_PARASOL;
      if (sscanf(optarg,"%d:%d,%d:%d", &min_row, &min_column, &max_row, &max_column) == 4 ) {
	/* do nothing */
      } else if (sscanf(optarg,"%d:%d",&min_row, &min_column) == 2) {
        max_row = min_row;
	max_column = min_column;
      } else if (sscanf(optarg,"%d,%d",&min_row, &max_row) == 2 ) {
	min_column = ROWCOL_UNDEFINED;
	max_column = ROWCOL_UNDEFINED;
      } else {	/* -C with invalid argument */
	fprintf(stderr, "%s : invalid argument for option -C (min_row[:min_column],max_row[:max_column]): %s\n", APPNAME, optarg);
	exit(EXIT_FAILURE);
      }
      if ((min_row > max_row) || (min_column > max_column)) {
      	fprintf(stderr, "%s : check your bounds for option -C (min_row[:min_column],max_row[:max_column]): %s\n", APPNAME, optarg);
	exit(EXIT_FAILURE);
      }
      break;
    case 'x' :
      flags |= DQX_DECIMAL;
      break;
    case 'g' :
      flags |= COORD_LATLON;
      break;
    case 'G' :
      {
        flags |= COORD_LATLON;
        if (sscanf(optarg,"%lf:%lf,%lf:%lf", &min_lat, &min_lon, &max_lat, &max_lon) == 4) {
	  /* do nothing */
	} else if (sscanf(optarg,"%lf:%lf", &min_lat, &min_lon) == 2) {
	  max_lat = min_lat;
	  max_lon = min_lon;
	} else if (sscanf(optarg,"%lf,%lf", &min_lat, &max_lat) == 2) {
	  min_lon = -180.;
	  max_lon = 180.;
	} else { /* -G with invalid argument */
	  fprintf(stderr, "%s : invalid argument for option -G (min_lat[:min_lon],max_lat[:max_lon]): %s\n", APPNAME, optarg);
	  exit(EXIT_FAILURE);
	}
	if ((min_lat > max_lat) || (min_lon > max_lon)) {
      	  fprintf(stderr, "%s : check your bounds for option -G (min_lat[:min_lon],max_lat[:max_lon]): %s\n", APPNAME, optarg);
	  exit(EXIT_FAILURE);
        }
	option_G_is_set = TRUE;
      }
      break;
    case 'P' :
      fprintf(stdout, "%s\n", PARAM_PATH);
      exit (EXIT_SUCCESS);
      break;
    case 'R' :
      {
        char res = optarg[0];
        if (res == 'm')      { user_res = MEDIUM_RESOLUTION; }
        else if (res == 'f') { user_res = FULL_RESOLUTION; }
        else {
          fprintf(stderr, "%s: fatal error: bad argument for -R: %s\n", APPNAME, optarg);
          exit (EXIT_FAILURE);
        }
      }
      break;
    case 'p' :
      strncpy(param_file, optarg, MAX_STRING_LENGTH);
      break;
    case 'X' :
      flags |= COORD_REVERSED;
      break;
    case 'T' : 
      memset(time_format, 0, sizeof(time_format));
      strncpy(time_format, optarg, sizeof(time_format) - 1);
      time_format_is_set = TRUE;
      break;
    case 't' :
      flags |= COORD_TIME;
      if (! time_format_is_set) {
	if (time_format[0] != '\0') { strcat(time_format, "\t"); } 
	strcat(time_format, DEFAULT_TIME_FORMAT); 
      }
      break;
    case 'd' :
      flags |= COORD_TIME;
      if (! time_format_is_set) { 
	if (time_format[0] != '\0') { strcat(time_format, "\t"); }
	strcat(time_format, DEFAULT_DATE_FORMAT); 
      }
      break;
    case 'N' :
      flags |= COORD_NODE_LON;
      break;
    case 'u' : /* undefined values (i.e. missing values or out of range values) */
      if (strcmp(optarg, "discard") == 0) {
        /* if the user asks for 'discard' explicitly, the lines containing at least one missing value will be discarded (former default behaviour) */
        flags &= ~OUTPUT_MISSING;
	flags &= ~OUTPUT_OUT_OF_RANGE;
      }
      else if (strcmp(optarg, "never-discard") == 0) {
	/* if the user asks for 'never-discard' explicitly, the lines containing only missing values won't be discarded (since 1.10.4) */
        flags |= OUTPUT_MISSING;
	flags |= OUTPUT_OUT_OF_RANGE;
	flags |= NEVER_DISCARD;
      }
      else {
        missing = out_of_range = atof(optarg);
        flags |= OUTPUT_MISSING;
        flags |= OUTPUT_OUT_OF_RANGE;
      }
      break;
    case 'm' : /* missing values */
      missing = atof(optarg);
      flags |= OUTPUT_MISSING;
      break;
    case 'r' : /* out of range values */
      out_of_range = atof(optarg);
      flags |= OUTPUT_OUT_OF_RANGE;
      break;
    case 'F' : /* output format */
      if ((strlen(optarg) > 2) && (optarg[0] == 'g') && (optarg[1] == ':')) {
	strncpy(geographic_coord_format, &optarg[2], sizeof(geographic_coord_format) - 1);
      }
      else {
	snprintf(output_format_temp, OUTPUT_FORMAT_MAXLEN, "%s\t", optarg);
      }
      break;
    case 'b' :
      flags |= RAW_MODE;
      break;
    case 'h' :
      if ((strstr(optarg, "TRGB") != NULL) 
          || (strstr(optarg, "TOGB") != NULL) || (strstr(optarg, "TOGC") != NULL) || (strstr(optarg, "TOGF") != NULL)
	  || (strstr(optarg, "TLGA") != NULL) || (strstr(optarg, "P3L3TLGB") != NULL) || (strstr(optarg, "TLGC") != NULL) 
	  || (strstr(optarg, "TBG1") != NULL) || (strstr(optarg, "TBGC") != NULL)
	  || (strstr(optarg, "TOGV") != NULL) || (strstr(optarg, "TOGW") != NULL) || (strstr(optarg, "TLGV") != NULL)) {
	sprintf(param_file, "%s/PARAM_%s.txt", PARAM_PATH, optarg);
	init_param(param_file);
	list_param();
      } else {
	fprintf(stderr, "%s : invalid argument for option -h (P[1-3]L1TBG[1-C], P[1-3]L[2-3]TRGB, P[1-3]L[2-3]TLGA, P3L3TLGB, P[1-3]L[2-3]TOGB, P[1-3]L[2-3]TOGC or P[1-3]L2T[OL]GV only)\n", APPNAME);
	exit(EXIT_FAILURE);
      }
      exit(EXIT_SUCCESS);
      break;
    case 'B' :
      format_dqx = FORMAT_DQX_ONE_BIT;
      dqx_bit_to_print = atof(optarg);
      if (dqx_bit_to_print < 1 || dqx_bit_to_print > 16) {
	fprintf(stderr, "%s : invalid argument for -%C (must be between 1 and 16)\n",APPNAME,optopt);
	exit(EXIT_FAILURE);
      }
      dqx_bit_to_print = 1 << (dqx_bit_to_print - 1);
      break;
    case 'D' :
      flags |= DEBUG_MODE;
      break;
    case '?' :
      fprintf(stderr,"%s : option %C : unknown\n\n",APPNAME, optopt);
      usage();
    default :
      fprintf(stderr,"%s : parse options : fatal error\n",APPNAME);
      exit(8);
    } /* switch */
  } /* while */
  *argc -= optind - 1;
  *argv += optind - 1;
  return flags;
}

/* pour un parametre de nom "param_name" en entree, renvoie :
 * param_ind : indice du parametre
 * param_pos : position du parametre (en octets relatifs par rapport au debut de l'enregistrement
 *   des donnees)
 * param_size : la taille du parametre en octets (une valeur negative represente un type signe, positive un type non signe)
 *
 * l'indice de parametre est egalement retourne comme valeur de get_param
 * (-1 en cas d'echec)
 */
int get_param(const char *param_name, int *param_ind, int *param_pos, int *param_size) {
  int i;
  char *strdir;
  char param_prefix[MAX_STRING_LENGTH + 1];
	
  memset(param_prefix, 0, sizeof(param_prefix));
	
  for (i=0 ; i<=max_param ; i++) {
    if (strcmp(param_name, param_names[i]) == 0) {
      *param_ind = param_indexes[i];
      *param_pos = param_positions[i];
      *param_size = param_sizes[i];
      return i;
    }
  }
  return -1;
}

int main(int argc, char *argv[]) {

  parasol_leader_scaling_factors_t parasol_leader_scaling_factors;
  parasol_data_desc_t parasol_data_desc;
  parasol_data_record_t parasol_data_record;
  float phys_value;
  unsigned char encoded_param_uchar;
  unsigned short encoded_param_ushort;
  unsigned long encoded_param_ulong;
  signed char encoded_param_schar;
  signed short encoded_param_sshort;
  signed long encoded_param_slong;
  char output_line[MAX_OUTPUT_LENGTH + 1];
  char token[MAX_STRING_LENGTH + 1];
  char param_name[NMAX_PARAMS][MAX_STRING_LENGTH + 1];
  int param_ind[NMAX_PARAMS]; 
  int param_pos[NMAX_PARAMS];
  int param_size[NMAX_PARAMS];
  unsigned long record, record_offset;
  unsigned long scaling_factors_offset;
  unsigned long leader_annotation_offset;
  unsigned char resolution;
  unsigned char parasol_level;
  float parameter_offset[NMAX_PARAMS], parameter_slope[NMAX_PARAMS];
  char leader_shortname[MAX_STRING_LENGTH + 1], data_shortname[MAX_STRING_LENGTH + 1];
  char leader_file[MAX_STRING_LENGTH + 1], data_file[MAX_STRING_LENGTH + 1];
	
  char cstr_first_time[MAX_STRING_LENGTH + 1];
  char cstr_last_time[MAX_STRING_LENGTH + 1];
  char cstr_ref_time[MAX_STRING_LENGTH + 1];
  char cstr_current_time[MAX_STRING_LENGTH + 1];
  char first_date[DATE_LEN+1]; /* premiere date d'acquisition d'une orbite PARASOL (pour l'option -t) */
  char last_date[DATE_LEN+1]; /* derniere date d'acquisition d'une orbite PARASOL (pour l'option -t) */
  char ref_date[DATE_LEN+1]; /* date de reference d'une orbite PARASOL (i.e. la date de noeud pour un niveau 1 ou 2) */
  char node_lon[NODE_LON_LEN+1];
  char cstr_northern_row[MAX_STRING_LENGTH + 1];
  char cstr_southern_row[MAX_STRING_LENGTH + 1];
  int northern_row;
  int southern_row;
  int nparams;
  size_t nread;
  int err;
	
  time_t first_time_t = (time_t) -1;
  time_t last_time_t = (time_t) -1;
  time_t ref_time_t = (time_t) -1;
  time_t node_time_t = (time_t) -1;

  time_t northern_time_t = (time_t) -1;
  time_t southern_time_t = (time_t) -1;

  int option_flags ; /* drapeau d'options */
  char *separator;
  int k;
  int values; /* nombre de valeurs dans la ligne d'affichage courante
	       * (ne pas afficher de ligne en l'absence de valeurs)
	       */
  int rec_offset=0;

  /* pour la determination du sens de stockage (nord-sud ou sud-nord) des enregistrements
   * (on determine la croissance ou la decroissance des lignes d'enregistrement a partir des
   * deux premieres)
   */
  char cstr_nrows_with_pixels[5];
  int nrows_with_pixels;

  int rec_min=0; /* premier enregistrement a lire dans le fichier */

  int irow;
	
  FILE *leader_fp ; /* Leader file pointer */
  FILE *data_fp ; /* Data file pointer */

  /* pour la determination de file_type */
  char basec[MAX_STRING_LENGTH + 1];
  char dirc[MAX_STRING_LENGTH + 1];
  char *bname;
  char *dname;
  char str_file_type[9]; /* P[1-3]L1TBG1, P[1-3]L1TBGC, P[1-3]L2TRGB, P[1-3]L2TOGB, P[1-3]L2TOGC, P[1-3]L2TOGV, 
		      * P[1-3]L2TLGA, P[1-3]L2TLGC, P[1-3]L2TLGV, P[1-3]L3TLGC, P[1-3]L3TOGC, P[1-3]L3TLGA, 
		      * P[1-3]L3TRGB 
		      */
  char polder_version; /* defined by str_file_type[1] */
  double rows_per_second;
  int file_type;
  
  param_file[0] = '\0'; /* makes sure param_file is empty (and not in an undefined state) at the beginning of the program */
  option_flags = parse_options(&argc, &argv);
   
  if (argc < 2) usage();
  
  memset(str_file_type, 0, sizeof(str_file_type));
  strncpy(basec, argv[1], sizeof(basec) - 1);
  strncpy(dirc, argv[1], sizeof(dirc) - 1);
  bname = basename(basec);
  dname = dirname(dirc);
  
  /* verifier que l'on donne bien un fichier leader (16e caractere == L) ou data (16e caractere == D) en argument
     supprimer le D ou le L terminal */
  if ((bname[15] != 'D') && (bname[15] != 'L')) {
    fprintf(stderr, "%s : invalid product name (must contain D or L at the 16th position)\n", argv[1]);
    exit (EXIT_FAILURE);
  }
  
  /* reformer les noms de fichiers leader et data a partir de chaque argument */
  sprintf(leader_shortname, "%s", bname);
  sprintf(data_shortname, "%s", bname);
 
  leader_shortname[15] = 'L';
  data_shortname[15] = 'D';

  sprintf(leader_file, "%s/%s", dname, leader_shortname);
  sprintf(data_file, "%s/%s", dname, data_shortname);
  
  strncpy(str_file_type, &bname[0], 8); /* determination du type de fichier */
  polder_version = str_file_type[1];
  if (('1' <= polder_version && polder_version <= '3') == FALSE) {
    fprintf(stderr, "%s: this version of %s supports only POLDER-1, POLDER-2, and POLDER-3 (PARASOL), not POLDER-%c\n",
	    APPNAME, APPNAME, polder_version);
    exit (EXIT_FAILURE);
  }
    
  file_type = FT_UNKNOWN;
  /* verifier le type de produit pour preparer son decodage */
  if (strstr(str_file_type, "L2TRGB") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
    file_type = FT_L2TRGB;
  } else if (strstr(str_file_type, "L2TOGB") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
    file_type = FT_L2TOGB;
  } else if (strstr(str_file_type, "L2TOGC") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
    file_type = FT_L2TOGC;
  } else if (strstr(str_file_type, "L2TOGF") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
    file_type = FT_L2TOGF;
  } else if (strstr(str_file_type, "L2TOGV") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
    file_type = FT_L2TOGV;
  } else if (strstr(str_file_type, "L2TOGW") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
    file_type = FT_L2TOGW;
  } else if (strstr(str_file_type, "L2TLGC") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
    file_type = FT_L2TLGC;
  } else if (strstr(str_file_type, "L2TLGF") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
    file_type = FT_L2TLGF;
  } else if (strstr(str_file_type, "L2TLGV") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
    file_type = FT_L2TLGV;
  } else if (strstr(str_file_type, "L2TLGW") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
    file_type = FT_L2TLGW;
  } else if (strstr(str_file_type, "L2TLGA") != NULL) {
    scaling_factors_offset = LEVEL2_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL2_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL2;
    file_type = FT_L2TLGA;
  } else if (strstr(str_file_type, "L1TBG1") != NULL) {
    scaling_factors_offset = LEVEL1_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL1_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL1;
    file_type = FT_L1TBG1;
  } else if (strstr(str_file_type, "L1TBGC") != NULL) {
    scaling_factors_offset = LEVEL1_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL1_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL1;
    file_type = FT_L1TBGC;
  } else if (strstr(str_file_type, "L3TLGC") != NULL) {
    scaling_factors_offset = LEVEL3_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL3_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL3;
    file_type = FT_L3TLGC;
  } else if (strstr(str_file_type, "L3TOGC") != NULL) {
    scaling_factors_offset = LEVEL3_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL3_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL3;
    file_type = FT_L3TOGC;
  } else if (strstr(str_file_type, "L3TLGA") != NULL) {
    scaling_factors_offset = LEVEL3_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL3_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL3;
    file_type = FT_L3TLGA;
  } else if (strstr(str_file_type, "L3TLGB") != NULL) {
    scaling_factors_offset = LEVEL3_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL3_LEADER_ANNOTATION_OFFSET;
    resolution = FULL_RESOLUTION;
    parasol_level = PARASOL_LEVEL3;
    file_type = FT_L3TLGB;
  } else if (strstr(str_file_type, "L3TRGB") != NULL) {
    scaling_factors_offset = LEVEL3_SCALING_FACTORS_OFFSET;
    leader_annotation_offset = LEVEL3_LEADER_ANNOTATION_OFFSET;
    resolution = MEDIUM_RESOLUTION;
    parasol_level = PARASOL_LEVEL3;
    file_type = FT_L3TRGB;
  } else {
    fprintf(stderr, "%s: %s: unknown or unimplemented product (available today: L1TBG1, L1TBGC, L2TRGB, L2TOGB, L2TOGC, L2TOGV, L2TLGA, L2TLGC, L2TLGV, L3TLGC, L3TOGC, L3TLGA, L3TRGB)\n", APPNAME, argv[1]);
    exit (EXIT_FAILURE);
  }
  
  if (file_type == FT_UNKNOWN) {
    fprintf(stderr, "%s: internal bug: file_type not initialized\n", APPNAME);
    exit (EXIT_FAILURE);
  }

  /* if the user chose to force the resolution (with the -R option), let it be */
  if (user_res == MEDIUM_RESOLUTION)    { resolution = MEDIUM_RESOLUTION; }
  else if (user_res == FULL_RESOLUTION) { resolution = FULL_RESOLUTION; }
  else { assert(user_res == 0); /* any other value is invalid */ }

  /* Obtention du nombre de lignes et colonnes maximales dans la grille du produit considere. */	
     
  switch (resolution) {
  case MEDIUM_RESOLUTION :
    if (min_row == ROWCOL_UNDEFINED) { min_row = 1; }
    if (min_column == ROWCOL_UNDEFINED) { min_column = 1; }
    if (max_row == ROWCOL_UNDEFINED) { max_row = N_ROWS_MEDIUMRES; }
    if (max_column == ROWCOL_UNDEFINED) { max_column = N_COLUMNS_MEDIUMRES; }
    
    if (   ((1 <= min_row && min_row <= N_ROWS_MEDIUMRES) == FALSE)
        || ((1 <= min_column && min_column <= N_COLUMNS_MEDIUMRES) == FALSE)
	|| ((1 <= max_row && max_row <= N_ROWS_MEDIUMRES) == FALSE)
        || ((1 <= max_column && max_column <= N_COLUMNS_MEDIUMRES) == FALSE)
	) {
      fprintf(stderr, "%s: invalid user-specified range (out of the medium resolution bounds): [%d:%d,%d:%d]\n",
              APPNAME, min_row, min_column, max_row, max_column);
      exit (EXIT_FAILURE);
    }
    nrows = N_ROWS_MEDIUMRES;
    ncolumns = N_COLUMNS_MEDIUMRES;
    break;
  case FULL_RESOLUTION :
    if (min_row == ROWCOL_UNDEFINED) { min_row = 1; }
    if (min_column == ROWCOL_UNDEFINED) { min_column = 1; }
    if (max_row == ROWCOL_UNDEFINED) { max_row = N_ROWS_FULLRES; }
    if (max_column == ROWCOL_UNDEFINED) { max_column = N_COLUMNS_FULLRES; }
    
    if (   ((1 <= min_row && min_row <= N_ROWS_FULLRES) == FALSE)
        || ((1 <= min_column && min_column <= N_COLUMNS_FULLRES) == FALSE)
	|| ((1 <= max_row && max_row <= N_ROWS_FULLRES) == FALSE)
        || ((1 <= max_column && max_column <= N_COLUMNS_FULLRES) == FALSE)
	) {
      fprintf(stderr, "%s: invalid user-specified range (out of the full resolution bounds): [%d:%d,%d:%d]\n",
              APPNAME, min_row, min_column, max_row, max_column);
      exit (EXIT_FAILURE);
    }
    nrows = N_ROWS_FULLRES;
    ncolumns = N_COLUMNS_FULLRES;
    break;
  default :
    fprintf(stderr, "%s: internal bug: unexpected value for resolution: %d\n", APPNAME, resolution);
    exit (EXIT_FAILURE);
    break;
  }
			
  /* Ouverture du fichier leader */
  leader_fp = fopen((const char *) leader_file, "rb");
  if (leader_fp == NULL) {
    fprintf(stderr, "%s: ", APPNAME);
    perror(leader_file);
    exit(2);
  }
  /* Ouverture du fichier data */
  data_fp = fopen((const char *) data_file, "rb");
  if (data_fp == NULL) {
    fprintf(stderr, "%s: ", APPNAME);
    perror(data_file);
    exit(3);
  }

  if (param_file[0] == '\0') {
    // if no parameter file was given with the -p option, determines the file automatically 
    set_param_file(param_file, data_file, data_fp, str_file_type);
  }
  init_param(param_file);

  /* Lecture des dates d'acquisition de debut et de fin d'orbite */
  if ((option_flags & COORD_TIME) || (option_flags & COORD_NODE_LON)) {
    size_t nread;
    struct tm first_tm;
    struct tm last_tm;
    struct tm ref_tm;

    switch (parasol_level) {
    case PARASOL_LEVEL1 :
    case PARASOL_LEVEL2 :
      if (fseek(leader_fp, POLDER_LEV1_LEV2_FIRST_DATE_OFFSET, SEEK_SET) != 0) {
	fprintf(stderr, "%s: %s: %s\n", APPNAME, leader_file, strerror(errno));
	exit(EXIT_FAILURE);
      }
	
      memset(first_date, 0, sizeof(first_date));
      nread = fread(first_date, sizeof(first_date) - 1, 1, leader_fp);
      if (nread < 1) {
	fprintf(stderr, "%s: %s: failed to read the first date of acquisition\n", APPNAME, leader_file);
	exit(EXIT_FAILURE);
      }
		
      if (fseek(leader_fp, POLDER_LEV1_LEV2_LAST_DATE_OFFSET, SEEK_SET) != 0) {
	fprintf(stderr, "%s: %s: %s\n", APPNAME, leader_file, strerror(errno));
	exit(EXIT_FAILURE);
      }
      memset(last_date, 0, sizeof(last_date));
      nread = fread(last_date, sizeof(last_date) - 1, 1, leader_fp);
      if (nread < 1) {
	fprintf(stderr, "%s: %s: failed to read the last date of acquisition\n", APPNAME, leader_file);
	exit(EXIT_FAILURE);
      }		
		
      if (fseek(leader_fp, POLDER_LEV1_LEV2_NODE_LON_OFFSET, SEEK_SET) != 0) {
	fprintf(stderr, "%s: %s: %s\n", APPNAME, leader_file, strerror(errno));
	exit(EXIT_FAILURE);
      }

      memset(node_lon, 0, sizeof(node_lon));
      nread = fread(node_lon, sizeof(node_lon) - 1, 1, leader_fp);
      if (nread < 1) {
	fprintf(stderr, "%s: %s: failed to read the node longitude\n", APPNAME, leader_file);
	exit(EXIT_FAILURE);
      }

      if (fseek(leader_fp, POLDER_LEV1_LEV2_NODE_DATE_OFFSET, SEEK_SET) != 0) {
	fprintf(stderr, "%s: %s: %s\n", APPNAME, leader_file, strerror(errno));
	exit(EXIT_FAILURE);
      }
		
      memset(ref_date, 0, sizeof(ref_date));
      nread = fread(ref_date, sizeof(ref_date) - 1, 1, leader_fp);
      if (nread < 1) {
	fprintf(stderr, "%s: %s: failed to read the node time\n", APPNAME, leader_file);
	exit(EXIT_FAILURE);
      }
		
      break;
    case PARASOL_LEVEL3 :
      if (fseek(leader_fp, POLDER_LEV3_SYNTH_FIRST_DATE_OFFSET, SEEK_SET) != 0) {
	fprintf(stderr, "%s: %s: %s\n", APPNAME, leader_file, strerror(errno));
	exit(EXIT_FAILURE);
      }
	
      memset(first_date, 0, sizeof(first_date));
				
      nread = fread(first_date, sizeof(first_date) - 1, 1, leader_fp);
      if (nread < 1) {
	fprintf(stderr, "%s: %s: failed to read the first date of synthesis\n", APPNAME, leader_file);
	exit(EXIT_FAILURE);
      }
		
      if (fseek(leader_fp, POLDER_LEV3_SYNTH_LAST_DATE_OFFSET, SEEK_SET) != 0) {
	fprintf(stderr, "%s: %s: %s\n", APPNAME, leader_file, strerror(errno));
	exit(EXIT_FAILURE);
      }
	
      memset(last_date, 0, sizeof(last_date));
				
      nread = fread(last_date, sizeof(last_date) - 1, 1, leader_fp);
      if (nread < 1) {
	fprintf(stderr, "%s: %s: failed to read the last date of synthesis\n", APPNAME, leader_file);
	exit(EXIT_FAILURE);
      }
		
      if (fseek(leader_fp, POLDER_LEV3_SYNTH_REF_DATE_OFFSET, SEEK_SET) != 0) {
	fprintf(stderr, "%s: %s: %s\n", APPNAME, leader_file, strerror(errno));
	exit(EXIT_FAILURE);
      }
	
      memset(ref_date, 0, sizeof(ref_date));
				
      nread = fread(ref_date, sizeof(ref_date) - 1, 1, leader_fp);
      if (nread < 1) {
	fprintf(stderr, "%s: %s: failed to read the reference date of synthesis\n", APPNAME, leader_file);
	exit(EXIT_FAILURE);
      }
		
      break;
    default :
      fprintf(stderr, "%s : internal bug : unexpected value for parasol_level : %d (contact the maintainer)\n", APPNAME, parasol_level);
      exit (EXIT_FAILURE);
      break;
    } /* switch (parasol_level) */
	
    if (sscanf(first_date, "%4d%2d%2d%2d%2d%2d", &first_tm.tm_year, &first_tm.tm_mon, &first_tm.tm_mday, 
	       &first_tm.tm_hour, &first_tm.tm_min, &first_tm.tm_sec) != 6) {
      fprintf(stderr, "%s: %s: failed to parse individual fields from start time\n", APPNAME, leader_file);
      exit(EXIT_FAILURE);
    }
    if (sscanf(last_date, "%4d%2d%2d%2d%2d%2d", &last_tm.tm_year, &last_tm.tm_mon, &last_tm.tm_mday, 
	       &last_tm.tm_hour, &last_tm.tm_min, &last_tm.tm_sec) != 6) {
      fprintf(stderr, "%s: %s: failed to parse individual fields from last time\n", APPNAME, leader_file);
      exit(EXIT_FAILURE);
    }
    if (sscanf(ref_date, "%4d%2d%2d%2d%2d%2d", &ref_tm.tm_year, &ref_tm.tm_mon, &ref_tm.tm_mday, 
	       &ref_tm.tm_hour, &ref_tm.tm_min, &ref_tm.tm_sec) != 6) {
      fprintf(stderr, "%s: %s: failed to parse individual fields from reference time\n", APPNAME, leader_file);
      exit(EXIT_FAILURE);
    }
	
    first_tm.tm_year -= 1900;
    last_tm.tm_year -= 1900;
    ref_tm.tm_year -= 1900;
    first_tm.tm_mon--;
    last_tm.tm_mon--;
    ref_tm.tm_mon--;
    first_tm.tm_isdst = 0;
    last_tm.tm_isdst = 0;
    ref_tm.tm_isdst = 0;
	
    first_time_t = my_timegm(&first_tm);
    last_time_t = my_timegm(&last_tm);
    ref_time_t = my_timegm(&ref_tm);
     
    if (argc == 2) { /* le seul argument est un nom de fichier, sans parametre a extraire */
      
      /* l'utilisateur a active l'option -t ou -d, afficher les heures de debut, de passage a l'equateur et de fin */
      if (option_flags & COORD_TIME) {
	if (! time_format_is_set) {
	  strncpy(time_format, "%F\t%H:%M:%S", sizeof(time_format) - 1);
	}
	strftime(cstr_first_time, sizeof(cstr_first_time), time_format, &first_tm);
	strftime(cstr_last_time, sizeof(cstr_last_time), time_format, &last_tm);
	strftime(cstr_ref_time, sizeof(cstr_ref_time), time_format, &ref_tm);
	fprintf(stdout, "%s\t%s\t%s\n", cstr_first_time, cstr_ref_time, cstr_last_time);
	exit (EXIT_SUCCESS);
      }
      
      /* l'utilisateur a active l'option -N, mais seulement sur un niveau 1 ou 2 (sur un niveau 3 cette option n'a aucun sens) */
      if ((parasol_level == PARASOL_LEVEL1 || parasol_level == PARASOL_LEVEL2) && (option_flags & COORD_NODE_LON)) {
	double dbl_node_lon = atof(node_lon);
	if (dbl_node_lon > 180.) { dbl_node_lon -= 360.; }
	fprintf(stdout, "%.3f\n", dbl_node_lon);
	exit (EXIT_SUCCESS);
      }
    } /* if (argc == 2) */
    
  } /* if ((option_flags & COORD_TIME) || (option_flags & COORD_NODE_LON)) */
  	
  if (argc == 2) { /* le seul argument est un nom de fichier, sans parametre a extraire
		    * on affiche la liste des parametres extractibles
		    */
    init_param(param_file);
    list_param();
    exit (EXIT_SUCCESS);
  }

  /* Lecture du fichier data, obtention du sens d'ecriture des lignes de donnees. */
  memset(&parasol_data_desc, 0, sizeof(parasol_data_desc));
	
  record_offset = DATA_RECORD_OFFSET;

  err = fseek(data_fp, record_offset, SEEK_SET);
	
  nread = fread(&parasol_data_record.record_number, sizeof(int), 1, data_fp);
  nread = fread(&parasol_data_record.record_length, sizeof(short), 1, data_fp);
  nread = fread(&parasol_data_record.line, sizeof(short), 1, data_fp);

  parasol_data_record.record_length = ntohs(parasol_data_record.record_length);

  /* determine le sens de stockage des donnees
   * lignes stockees par valeurs croissantes dans la grille (donc du nord au sud)
   * ou par valeurs decroissantes dans la grille (donc du sud au nord)
   * attribution de la premiere et derniere heure d'acquisition soit a la premiere ligne (la plus au nord),
   * soit a la derniere ligne (la plus au sud) de la grille selon le cas
   */
  
  memset(cstr_nrows_with_pixels, 0, sizeof(cstr_nrows_with_pixels));
  err = fseek(leader_fp, leader_annotation_offset + 200 /* number of non-empty rows, see PARASOL specification file */, SEEK_SET);
  nread = fread(cstr_nrows_with_pixels, 4*sizeof(char), 1, leader_fp);
  nrows_with_pixels = atoi(cstr_nrows_with_pixels);

  memset(cstr_northern_row, 0, sizeof(cstr_northern_row));
  err = fseek(leader_fp, NORTHERN_PIXEL_ROW_OFFSET, SEEK_SET);
  nread = fread(cstr_northern_row, 4*sizeof(char), 1, leader_fp);
  err = fseek(leader_fp, SOUTHERN_PIXEL_ROW_OFFSET, SEEK_SET);
  nread = fread(cstr_southern_row, 4*sizeof(char), 1, leader_fp);
  
  if (parasol_level == PARASOL_LEVEL1) {
    northern_row = atoi(cstr_northern_row);
    southern_row = atoi(cstr_southern_row);
  }
  else if (parasol_level == PARASOL_LEVEL2 || parasol_level == PARASOL_LEVEL3) {
    /* pour le niveaux 2 et 3, les champs cstr_southern_row et cstr_northern_row sont
     * mal renseignes (ils contiennent la premiere et la derniere ligne contenant des donnees
     * de niveau 2 ou 3, et non la premiere et derniere ligne d'acquisition au niveau 1, donc
     * sont inexploitables pour l'interpolation temporelle : il serait errone de leur attribuer
     * southern_time_t et northern_time_t). On choisit de prendre les lignes extremes de
     * la grille
     */
    northern_row = 1;
    southern_row = nrows;
  }
  else {
    fprintf(stderr, "%s: internal bug: unexpected parasol_level value: %d (should never have displayed this message)\n",
	    APPNAME, parasol_level);
    exit (EXIT_FAILURE);
  }
  
  if (option_flags & COORD_PARASOL_PIX_AND_SPX) {
    if (resolution != FULL_RESOLUTION) {
      fprintf(stderr, "%s: %s: not a full resolution product, can't be used with the -S option\n", APPNAME, data_file);
      exit(EXIT_FAILURE);
    }
    /* disables COORD_PARASOL when COORD_PARASOL_PIX_AND_SPX is active */
    option_flags &= ~COORD_PARASOL;
  }

  if (option_flags & COORD_TIME) {
    if (polder_version == '1' || polder_version == '2') {
      northern_time_t = first_time_t;
      southern_time_t = last_time_t;
    }
    else if (polder_version == '3') {
      southern_time_t = first_time_t;
      northern_time_t = last_time_t;
    }
    else {
      fprintf(stderr, "%s: internal bug: unexpected polder_version value: %c (should never have displayed this message)\n",
	      APPNAME, polder_version);
      exit (EXIT_FAILURE);
    }
    
    if (northern_time_t == southern_time_t) {
      fprintf(stderr, "%s: %s: invalid southern and northern times (are the same): %d and %d\n", APPNAME, data_file, (int) southern_time_t, (int) northern_time_t);
      exit (EXIT_FAILURE);
    }
    rows_per_second = ((double) northern_row - southern_row)/(northern_time_t - southern_time_t);
    assert(((polder_version == '1' || polder_version == '2') && rows_per_second > 0)
           || (polder_version == '3' && rows_per_second < 0));
  }

  if (argc - 2 >= NMAX_PARAMS) {
    fprintf(stderr, "%s: too many arguments\n", APPNAME);
    exit (EXIT_FAILURE);
  }
  
  /* loads parameters from the command line, expands wildcards if necessary (_dir* will be expanded into _dir01, _dir02, ... _dir14, _dir15, _dir16) */
  nparams = 0;
  for (k = 0 ; k < argc - 2 ; k++) {
    if (strstr(argv[k+2], "_dir*")) { /* found a wildcard, expand it */
       int ndirs;
       int idir;
       
       if (polder_version == '1' || polder_version == '2') {
         ndirs = 14;
       }
       else if (polder_version == '3') {
         ndirs = 16;
       }
       else {
         fprintf(stderr, "%s: unexpected polder version: %c (internal bug, should never have reached this point)\n", APPNAME, polder_version);
	 exit (EXIT_FAILURE);
       }
       for (idir = 0 ; idir < ndirs ; idir++) {
         char param_prefix[MAX_STRING_LENGTH + 1];
	 strncpy(param_prefix, argv[k+2], MAX_STRING_LENGTH);
	 char *pUnderscore = rindex(param_prefix, '_');
	 assert(pUnderscore != NULL);
	 *pUnderscore = '\0'; /* removes the suffix '_dir' in param_prefix */
	 sprintf(param_name[nparams++], "%s_dir%2.2d", param_prefix, idir + 1); /* replaces it by a numbered prefix */
       }
    }
    else {
      strncpy(param_name[nparams++], argv[k+2], MAX_STRING_LENGTH);
    }
  }  
  	
  for (k = 0 ; k < nparams ; k++) {
	
    if (get_param(param_name[k], &param_ind[k], &param_pos[k], &param_size[k]) == -1) {
      fprintf(stderr,"%s: %s: unknown parameter. Please check parameters from file %s\n",APPNAME, param_name[k], param_file);
      exit(7);
    }
    
    memset(&parasol_leader_scaling_factors, (char) 0, sizeof(parasol_leader_scaling_factors));
    
    err   = fseek(leader_fp, scaling_factors_offset, SEEK_SET);
    nread = fread(&parasol_leader_scaling_factors.record_number, sizeof(int), 1, leader_fp);
    nread = fread(&parasol_leader_scaling_factors.record_length, sizeof(int), 1, leader_fp);
    nread = fread(&parasol_leader_scaling_factors.interleaving, 8*sizeof(char), 1, leader_fp);
    nread = fread(&parasol_leader_scaling_factors.byte_ordering, 16*sizeof(char), 1, leader_fp);
    nread = fread(&parasol_leader_scaling_factors.npar, 4*sizeof(char), 1, leader_fp);
    nread = fread(&parasol_leader_scaling_factors.nbytes, 8*sizeof(char), 1, leader_fp);
	
    parasol_leader_scaling_factors.record_number = ntohl(parasol_leader_scaling_factors.record_number);
    parasol_leader_scaling_factors.record_length = ntohl(parasol_leader_scaling_factors.record_length);
	
    err   = fseek(leader_fp, scaling_factors_offset + 26*param_ind[k] + 20, SEEK_SET);
				
    nread = fread(&parasol_leader_scaling_factors.slope, 12*sizeof(char), 1, leader_fp);
    nread = fread(&parasol_leader_scaling_factors.offset, 12*sizeof(char), 1, leader_fp);
	
    parameter_slope[k] = (float) atof(parasol_leader_scaling_factors.slope);
    parameter_offset[k] = (float) atof(parasol_leader_scaling_factors.offset);
		
		
		
    if (option_flags & DEBUG_MODE) {
      fprintf(stderr, "%s : (%i) : %i : param_name : %s  param_ind : %i  "
	      "param_pos : %i  param_size : %i\n",APPNAME,
	      __LINE__, k, param_name[k], param_ind[k], param_pos[k], param_size[k]);
      fprintf(stderr, "%s : (%i) : slope : %f  offset : %f\n",APPNAME,
	      __LINE__, parameter_slope[k], parameter_offset[k]);
    }
		
  } /* for k */
	
  fclose(leader_fp); /* fermeture du leader file */
	
  if (option_flags & DEBUG_MODE) exit(EXIT_FAILURE);
	
  /* Lecture du fichier data */
  memset(&parasol_data_desc, (char) 0, sizeof(parasol_data_desc));
	
  err   = fseek(data_fp, 0, SEEK_SET);
  nread = fread(&parasol_data_desc.record_number, sizeof(int), 1, data_fp);
  nread = fread(&parasol_data_desc.record_length, sizeof(int), 1, data_fp);
  nread = fread(&parasol_data_desc.control_doc_number, 12*sizeof(char), 1, data_fp);
  nread = fread(&parasol_data_desc.control_doc_version, 6*sizeof(char), 1, data_fp);
  nread = fread(&parasol_data_desc.soft_version_number, 6*sizeof(char), 1, data_fp);
  nread = fread(&parasol_data_desc.file_number, 4*sizeof(char), 1, data_fp);
  nread = fread(&parasol_data_desc.file_name, 16*sizeof(char), 1, data_fp);
  nread = fread(&parasol_data_desc.data_rec_number, sizeof(int), 1, data_fp);
  nread = fread(&parasol_data_desc.data_rec_length, sizeof(int), 1, data_fp);
  nread = fread(&parasol_data_desc.spare1, 40*sizeof(char), 1, data_fp);
  nread = fread(&parasol_data_desc.data_prefix_length, sizeof(int), 1, data_fp);
  nread = fread(&parasol_data_desc.data_length, sizeof(int), 1, data_fp);
  nread = fread(&parasol_data_desc.spare2, 68*sizeof(char), 1, data_fp);
	
  parasol_data_desc.record_number = ntohl(parasol_data_desc.record_number);
  parasol_data_desc.record_length = ntohl(parasol_data_desc.record_length);
  parasol_data_desc.data_rec_number = ntohl(parasol_data_desc.data_rec_number);
  parasol_data_desc.data_rec_length = ntohl(parasol_data_desc.data_rec_length);
  parasol_data_desc.data_prefix_length = ntohl(parasol_data_desc.data_prefix_length);
  parasol_data_desc.data_length = ntohl(parasol_data_desc.data_length);
	
  record_offset = DATA_RECORD_OFFSET+parasol_data_desc.data_rec_length*rec_min;
	
  for (record=rec_min; record<parasol_data_desc.data_rec_number; record++) {

    err   = fseek(data_fp, record_offset, SEEK_SET);
    nread = fread(&parasol_data_record.record_number, sizeof(int), 1, data_fp);
    nread = fread(&parasol_data_record.record_length, sizeof(short), 1, data_fp);
    nread = fread(&parasol_data_record.line, sizeof(short), 1, data_fp);
    nread = fread(&parasol_data_record.column, sizeof(short), 1, data_fp);

    parasol_data_record.record_number = ntohl(parasol_data_record.record_number);
    parasol_data_record.record_length = ntohs(parasol_data_record.record_length);
    parasol_data_record.line = ntohs(parasol_data_record.line);
    parasol_data_record.column = ntohs(parasol_data_record.column);
    
    if ((min_row <= parasol_data_record.line && parasol_data_record.line <= max_row) == FALSE) {
    	goto next_record;
    }
    if ((min_column <= parasol_data_record.column && parasol_data_record.column <= max_column) == FALSE) {
    	goto next_record;
    }
    
    output_line[0] = '\0'; /* reinitialiser output_line a la chaine vide */
		
    if (option_flags & COORD_PARASOL) {
      if (option_flags & COORD_REVERSED) {
	sprintf(token, "%d\t%d\t", parasol_data_record.column, parasol_data_record.line);
      }
      else {
	sprintf(token, "%d\t%d\t", parasol_data_record.line, parasol_data_record.column);
      }
      strcat(output_line, token);
    }
    
    if (option_flags & COORD_PARASOL_PIX_AND_SPX) {
      int err;
      double lat, lon;
      double line_spx, column_spx;
      
      
      err = ligcol2latlon_polder(parasol_data_record.line, parasol_data_record.column, POLDER_FULL_RESOLUTION, &lat, &lon);
      if (err != 0) {
        fprintf(stderr, "%s: %s: bad input coordinates: line: %d, column: %d\n", APPNAME, data_file, parasol_data_record.line, parasol_data_record.column);
	exit (EXIT_FAILURE);
      }
      err = latlon2ligcol_polder(lat, lon, POLDER_MEDIUM_RESOLUTION, &line_spx, &column_spx);
      if (err != 0) {
        fprintf(stderr, "%s: %s: bad input coordinates: lat: %f, lon: %f\n", APPNAME, data_file, lat, lon);
	exit (EXIT_FAILURE);
      }
      
      if (option_flags & COORD_REVERSED) {
        sprintf(token, "%d\t%d\t%.0f\t%.0f\t", parasol_data_record.column, parasol_data_record.line, column_spx, line_spx);
      }
      else {
        sprintf(token, "%d\t%d\t%.0f\t%.0f\t", parasol_data_record.line, parasol_data_record.column, line_spx, column_spx);
      }
      strcat(output_line, token);
      
    }
    
    if ((option_flags & COORD_LATLON) && (resolution == MEDIUM_RESOLUTION)) { 
      double lat;
      double lon;
      
      ligcol2latlon_polder(parasol_data_record.line, parasol_data_record.column, POLDER_MEDIUM_RESOLUTION, &lat, &lon);
      if ((min_lat <= lat && lat <= max_lat && min_lon <= lon && lon <= max_lon) == FALSE) {
	goto next_record;
      }
      
      if (option_flags & COORD_REVERSED) {
	sprintf(token, geographic_coord_format,lon,lat);
      }
      else {
	sprintf(token, geographic_coord_format,lat,lon);
      }
      strcat(output_line, token);
    }
    else if ((option_flags & COORD_LATLON) && (resolution == FULL_RESOLUTION)) {
      double lat;
      double lon;
      
      ligcol2latlon_polder(parasol_data_record.line, parasol_data_record.column, POLDER_FULL_RESOLUTION, &lat, &lon);
      if ((min_lat <= lat && lat <= max_lat && min_lon <= lon && lon <= max_lon) == FALSE) {
	goto next_record;
      }
      
      if (option_flags & COORD_REVERSED) {
	sprintf(token, geographic_coord_format,lon,lat);
      }
      else {
	sprintf(token, geographic_coord_format,lat,lon);
      }
      strcat(output_line, token);
    }
    if (option_flags & COORD_TIME) {
      time_t current_time_t;
      struct tm current_tm;
      double lat;
      double lon;
      int current_row = parasol_data_record.line;
      
      current_time_t = southern_time_t + ((double) current_row - southern_row)/rows_per_second;

      strftime(cstr_current_time, sizeof(cstr_current_time), time_format, gmtime(&current_time_t));
      sprintf(token, "%s\t", cstr_current_time);
      strcat(output_line, token);
    }
		
    values = 0;

    for (k = 0 ; k < nparams ; k++) {
			
      if ((file_type == FT_L1TBG1 || file_type == FT_L1TBGC) && strstr(param_name[k],"dqx_dir") != NULL) {
	err   = fseek(data_fp, record_offset + param_pos[k] - 1, SEEK_SET);
	nread = fread(&encoded_param_ushort, sizeof(short), 1, data_fp);
	encoded_param_ushort = ntohs(encoded_param_ushort);
	values++;
				
	if (dqx_bit_to_print == 0) { /* prints all bits of the dqx */
	  if (option_flags & DQX_DECIMAL) { /* in decimal form */
	    sprintf(token, "%d\t", encoded_param_ushort);
	  }
	  else { /* in binary form */
	    sprintf(token, format_dqx, 
		    (encoded_param_ushort & 0x8000 ? 1 : 0), /* bit 16 */
		    (encoded_param_ushort & 0x4000 ? 1 : 0), /* bit 15 */
		    (encoded_param_ushort & 0x2000 ? 1 : 0), /* bit 14 */
		    (encoded_param_ushort & 0x1000 ? 1 : 0), /* bit 13 */
		    (encoded_param_ushort & 0x800 ? 1 : 0),  /* bit 12 */
		    (encoded_param_ushort & 0x400 ? 1 : 0),  /* bit 11 */
		    (encoded_param_ushort & 0x200 ? 1 : 0),  /* bit 10 */
		    (encoded_param_ushort & 0x100 ? 1 : 0),  /* bit 9 */
		    (encoded_param_ushort & 0x80 ? 1 : 0),   /* bit 8 */
		    (encoded_param_ushort & 0x40 ? 1 : 0),   /* bit 7 */
		    (encoded_param_ushort & 0x20 ? 1 : 0),   /* bit 6 */
		    (encoded_param_ushort & 0x10 ? 1 : 0),   /* bit 5 */
		    (encoded_param_ushort & 0x8 ? 1 : 0),    /* bit 4 */
		    (encoded_param_ushort & 0x4 ? 1 : 0),    /* bit 3 */
		    (encoded_param_ushort & 0x2 ? 1 : 0),    /* bit 2 */
		    (encoded_param_ushort & 0x1 ? 1 : 0)     /* bit 1 */
		    );
	  }
	} else { /* prints only one bit (the dqx_bit_to_print) */
	  sprintf(token, format_dqx, (encoded_param_ushort & dqx_bit_to_print ? 1 : 0));
	}
	strcat(output_line, token);
	continue;
      }
#if 0
      else if ((file_type == FT_L2TOGC || file_type == FT_L2TOGV || file_type == FT_L2TOGF || file_type == FT_L2TOGW)
	       && strcmp(param_name[k], "dqs") == 0) {
	/* decoding dqs is not implemented (it will be displayed in raw value like in previous versions) */
      }
#endif
      
      if (k == nparams - 1) {
	separator = "";
      }
      else {
	separator = "\t";
      }
      sprintf(output_format,"%s%s",output_format_temp, separator);
	
      switch(param_size[k]) {
      case -1 : /* one-byte signed parameters */
	err   = fseek(data_fp, record_offset + param_pos[k] - 1, SEEK_SET);
	nread = fread(&encoded_param_schar, sizeof(char), 1, data_fp);
				
	/* one-byte signed parameters exist only for level-1 products (and no out-of-range value exist) */
	if ((parasol_level == PARASOL_LEVEL1) && (encoded_param_schar == PARASOL_LEVEL1_SI1_MISSING)) {
	  if (option_flags & OUTPUT_MISSING) {
	    sprintf(token, output_format, missing);
	    strcat(output_line, token);
	  } else {
	    goto next_record;
	  }
	  break;
	}
	values++;
	if (option_flags & RAW_MODE) { /* affiche la valeur brute (non decodee) */
	  sprintf(token, "%d%s", encoded_param_schar, separator);
	  strcat(output_line, token);
	  break;
	}
	phys_value = (float) encoded_param_schar;
	if (parameter_slope[k]) phys_value = parameter_offset[k] + parameter_slope[k]*phys_value;
	sprintf(token, output_format, phys_value);
	strcat(output_line, token);
	break;
      case 1 : /* one-byte unsigned parameters */
	err   = fseek(data_fp, record_offset + param_pos[k] - 1, SEEK_SET);
	nread = fread(&encoded_param_uchar, sizeof(char), 1, data_fp);
	if ((parasol_level == PARASOL_LEVEL1) && (encoded_param_uchar == PARASOL_LEVEL1_UI1_MISSING)) { 
	  /* no unsigned one-byte out-of-range value exist for level-1 */
	  if (option_flags & OUTPUT_MISSING) {
	    sprintf(token, output_format, missing);
	    strcat(output_line, token);
	  } else {
	    goto next_record;
	  }
	  break;
	} else if (parasol_level == PARASOL_LEVEL2 || parasol_level == PARASOL_LEVEL3) {
	  if (encoded_param_uchar == PARASOL_LEVEL2_UI1_MISSING) {
	    if (option_flags & OUTPUT_MISSING) {
	      sprintf(token, output_format, missing);
	      strcat(output_line, token);
	    } else {
	      goto next_record;
	    }
	    break;
	  } else if (encoded_param_uchar == PARASOL_LEVEL2_UI1_OUT_OF_RANGE) {
	    if (option_flags & OUTPUT_OUT_OF_RANGE) {
	      sprintf(token, output_format, out_of_range);
	      strcat(output_line, token);
	    } else {
	      goto next_record;
	    }
	    break;
	  }
	}
	values++;
	if (option_flags & RAW_MODE) { /* affiche la valeur brute (non decodee) */
	  sprintf(token, "%d%s", encoded_param_uchar, separator);
	  strcat(output_line, token);
	  break;
	}
	phys_value = (float) encoded_param_uchar;
	if (parameter_slope[k]) phys_value = parameter_offset[k] + parameter_slope[k]*phys_value;
	sprintf(token, output_format, phys_value);
	strcat(output_line, token);
	break;
      case -2 : /* two-bytes signed parameters */
	err   = fseek(data_fp, record_offset + param_pos[k] - 1, SEEK_SET);
	nread = fread(&encoded_param_sshort, sizeof(short), 1, data_fp);
	encoded_param_sshort = ntohs(encoded_param_sshort);
				
	/* two-byte signed parameters exist only for level-1 products */
	if (parasol_level == PARASOL_LEVEL1) {
	  if (encoded_param_sshort == PARASOL_LEVEL1_SI2_MISSING) {
	    if (option_flags & OUTPUT_MISSING) {
	      sprintf(token, output_format, missing);
	      strcat(output_line, token);
	    } else {
	      goto next_record;
	    }
	    break;
	  } else if (encoded_param_sshort == PARASOL_LEVEL1_SI2_OUT_OF_RANGE) {
	    if (option_flags & OUTPUT_OUT_OF_RANGE) {
	      sprintf(token, output_format, out_of_range);
	      strcat(output_line, token);
	    } else {
	      goto next_record;
	    }
	    break;
	  }
	}
	values++;
	if (option_flags & RAW_MODE) { /* affiche la valeur brute (non decodee) */
	  sprintf(token, "%d%s", encoded_param_sshort, separator);
	  strcat(output_line, token);
	  break;
	}
	phys_value = (float) encoded_param_sshort;
	if (parameter_slope[k]) phys_value = parameter_offset[k] + parameter_slope[k]*phys_value;
	sprintf(token, output_format, phys_value);
	strcat(output_line, token);
	break;
      case 2 : /* two-bytes unsigned parameters */
	err   = fseek(data_fp, record_offset + param_pos[k] - 1, SEEK_SET);
	nread = fread(&encoded_param_ushort, sizeof(short), 1, data_fp);
	encoded_param_ushort = ntohs(encoded_param_ushort);
	if ((parasol_level == PARASOL_LEVEL1) && (encoded_param_ushort == PARASOL_LEVEL1_UI2_MISSING)) {
	  /* no unsigned two-bytes out-of-range value exist for level-1 */
	  if (option_flags & OUTPUT_MISSING) {
	    sprintf(token, output_format, missing);
	    strcat(output_line, token);
	  } else {
	    goto next_record;
	  }
	  break;
	} else if (parasol_level == PARASOL_LEVEL2 || parasol_level == PARASOL_LEVEL3) {
	  if (encoded_param_ushort == PARASOL_LEVEL2_UI2_MISSING) {
	    if (option_flags & OUTPUT_MISSING) {
	      sprintf(token, output_format, missing);
	      strcat(output_line, token);
	    } else {
	      goto next_record;
	    }
	    break;
	  } else if (encoded_param_ushort == PARASOL_LEVEL2_UI2_OUT_OF_RANGE) {
	    if (option_flags & OUTPUT_OUT_OF_RANGE) {
	      sprintf(token, output_format, out_of_range);
	      strcat(output_line, token);
	    } else {
	      goto next_record;
	    }
	    break;
	  }
	}
	values++;
	if (option_flags & RAW_MODE) { /* affiche la valeur brute (non decodee) */
	  sprintf(token, "%d%s", encoded_param_ushort, separator);
	  strcat(output_line, token);
	  break;
	}
	phys_value = (float) encoded_param_ushort;
	if (parameter_slope[k]) phys_value = parameter_offset[k] + parameter_slope[k]*phys_value;
	sprintf(token, output_format, phys_value);
	strcat(output_line, token);
	break;
      case 4 :
	err   = fseek(data_fp, record_offset + param_pos[k] - 1, SEEK_SET);
	nread = fread(&encoded_param_ulong, sizeof(long), 1, data_fp);
	encoded_param_ulong = ntohl(encoded_param_ulong);
	/* Out of range or missing values for 4-bytes parameters are not documented, so won't be implemented */
	values++;
	if (option_flags & RAW_MODE) { /* affiche la valeur brute (non decodee) */
	  sprintf(token, "%lu%s", encoded_param_ulong, separator);
	  strcat(output_line, token);
	  break;
	}
	phys_value = (float) encoded_param_ulong;
	if (parameter_slope[k]) phys_value = parameter_offset[k] + parameter_slope[k]*phys_value;
	sprintf(token, output_format, phys_value);
	strcat(output_line, token);
	break;
      default :
	fprintf(stderr, "%s : %s : invalid parameter size (%d)\n"
		"Check file %s\n", APPNAME, param_name[k], param_size[k], param_file);
	exit(1);
      } /* switch */
    } /* for (k) */
    
    if (values || (option_flags & NEVER_DISCARD)) {
      printf("%s\n", output_line);
    }
  next_record:
    record_offset += parasol_data_record.record_length;

  } /* for (record) */
  fclose(data_fp); /* Fermeture du fichier data */
  exit(0);

} /* main */
