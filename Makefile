CC = cc

all : parasolascii polderascii ascii2map_medium ascii2map_full liste_param_codage map2ascii test_polder_geo

parasolascii : parasolascii.c parasolascii.h polder_geo.o polder_geo.h
	$(CC) -DAPPNAME=\"parasolascii\" "-DPARAM_PATH=\"${PARAM_DIR}\"" -O2 -o parasolascii parasolascii.c polder_geo.o -lm 

polderascii : parasolascii.c parasolascii.h polder_geo.o polder_geo.h
	$(CC) -DAPPNAME=\"polderascii\" "-DPARAM_PATH=\"${PARAM_DIR}\"" -O2 -o polderascii parasolascii.c polder_geo.o -lm 

polder_geo.o : polder_geo.c polder_geo.h
	$(CC) -O2 -c polder_geo.c

map2ascii : map2ascii.c
	$(CC) -o map2ascii map2ascii.c -lm

ascii2map_medium : ascii2map_medium.c
	$(CC) -o ascii2map_medium ascii2map_medium.c

ascii2map_full : ascii2map_full.c
	$(CC) -o ascii2map_full ascii2map_full.c

liste_param_codage : liste_param_codage.c
	$(CC) "-DPARAM_PATH=\"${PARAM_DIR}\"" -o liste_param_codage liste_param_codage.c

test_polder_geo : test_polder_geo.c polder_geo.o
	$(CC) -Wall -g -o test_polder_geo test_polder_geo.c polder_geo.o -lm

clean :
	rm -f core parasolascii parasolascii.exe \
        polderascii polderascii.exe liste_param_codage \
	test_polder_geo.exe test_polder_geo \
        ascii2map_medium ascii2map_full map2ascii *~ *.o
