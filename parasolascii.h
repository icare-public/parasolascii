/* parasolascii.h */

typedef struct _parasol_leader_scaling_factors_t {
	int record_number;
	int record_length;
	char interleaving[9];
	char byte_ordering[17];
	char npar[5];
	char nbytes[9];
	char slope[13];
	char offset[13];
} parasol_leader_scaling_factors_t;

typedef struct _parasol_data_desc_t {
	int record_number;
	int record_length;
	char control_doc_number[13];
	char control_doc_version[7];
	char soft_version_number[7];
	char file_number[5];
	char file_name[17];
	int data_rec_number;
	int data_rec_length;
	char spare1[40];
	int data_prefix_length;
	int data_length;
	char spare2[68];
} parasol_data_desc_t ;

typedef struct _parasol_data_non_dir_t {
	unsigned short dqstrait;
	unsigned char utc_hour;
	unsigned char utc_min;
	unsigned char Ni;
	unsigned int Nr : 4;
	unsigned int Np : 4;
	unsigned int min_glitter : 4;
	unsigned int max_glitter : 4;
	unsigned char mu0;
	short avis;
	char rsd_avis;
	char rad_avis;
	char qavis;
	short ascvis;
	char rad_ascvis;
	char avis_clair;
	short asw;
	char asw_clair;
	char cn;
	unsigned int delta_cn_plus : 4;
	unsigned int delta_cn_minus : 4;
	char qcn;
	char moyp_uh2o_app;
	char qh2o;
	char moypi_pnuage;
	char sigi_pnuage;
	char moypi_pray;
	char sigi_pray;
	short tau;
	char rsd_tau;
	char epsilon_tau;
	char asvis;
	char ind_phase;
	char ind_micro;
} parasol_data_non_dir_t;

typedef struct _parasol_data_dir_t {
	char moyp_thetav;
	char moyp_phi;
	short rvis;
	short avis;
	short rsw;
	short asw;
} parasol_data_dir_t;

typedef struct _parasol_data_record_t {
	int record_number;
	short record_length;
	unsigned short line;
	unsigned short column;
	signed short altitude;
	unsigned char surface_indicator; /* land, sea, coast */	
	parasol_data_non_dir_t parasol_data_non_dir;
} parasol_data_record_t ;
