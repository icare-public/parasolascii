/* ascii2map_full.c */

#include <stdio.h>
#include <stdlib.h>

#define UNDEF -999.
#define NB_LIGNES 3240
#define NB_COLONNES 6480

int main(int argc, char* argv[]) {

	char* output_file;
	FILE* input_handle;
	FILE* output_handle;
	int ligne_pix, colonne_pix;
	float valeur_pix;
	int i,j;
	float* image = (float*) malloc(sizeof(float)*NB_LIGNES*NB_COLONNES);

	if (argc == 2) {
		input_handle = stdin;
		output_file = argv[1];
	}
	else {
		fprintf(stderr,"_____________________________________________________\n\n");
		fprintf(stderr,"                     ascii2map_full                  \n");
		fprintf(stderr,"_____________________________________________________\n\n");
		
		fprintf(stderr,"usage : ascii2map_full output_raw_file < input_txt_file (or use the pipe, see below)\n\n");
		fprintf(stderr,"This tool creates a binary image from a parasolascii output of\n"
			       "three columns representing respectivily the line, the column and\n"
		               "the value of a parasol parameter of full resolution.\n\n");     
		fprintf(stderr,"The input data are passed from the standard input\n");
		fprintf(stderr,"The undefined values written in the output file are set to %.2f\n\n", UNDEF);
		fprintf(stderr,"Examples :\n\n");
		fprintf(stderr,"# creates an image file cc.raw from a text file cc.txt (3 columns i j cc):\n");
		fprintf(stderr,"$ ascii2map_full cc.raw < cc.txt\n\n");
		fprintf(stderr,"cc.raw is a raw binary file (extension .raw or .dat) with 6480x3240x(real 32 bits)\n\n");
		fprintf(stderr,"# creates a binary file cc.raw directly from parasolascii:\n");
		fprintf(stderr,"$ parasolascii -c PARASOL_DATA_FILE cc | ascii2map_full cc.raw\n\n");
		exit(1);
	}
	
	
	
	for (i=0 ; i<NB_LIGNES; i++)
		for (j=0 ; j<NB_COLONNES ; j++)
			image[i*NB_COLONNES+j] = UNDEF;
	
	while(! feof(input_handle)) {
		fscanf(input_handle,"%d %d %f", &ligne_pix, &colonne_pix, &valeur_pix);
		if ((1 <= ligne_pix && ligne_pix <= NB_LIGNES)
		    && (1 <= colonne_pix && colonne_pix <= NB_COLONNES)) {
		  image[(ligne_pix-1)*NB_COLONNES+(colonne_pix-1)] = valeur_pix;
		}
		else {
		  fprintf(stderr, "ascii2map_full: coordonnees invalides: (%d,%d)\n", ligne_pix, colonne_pix);
		  exit (EXIT_FAILURE);
		}

	}
	fclose(input_handle);
	
        output_handle = fopen((const char*) output_file,"w");
	if (output_handle == NULL) {
		perror(output_file);
		exit(2);
	}
	fwrite(image,sizeof(float),NB_LIGNES*NB_COLONNES,output_handle);
	fclose(output_handle);
	
	exit(0);

}
